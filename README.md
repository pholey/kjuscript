KJUScript
=========

Kim Jong Un's programming language.

Quick Start
===========

a quick reference guide for basic KJU stuff


Assigning variables:
--------------------

```ruby
KJU says x is now 3
KJU says y is now "hello"
y #=> "hello"
x #=> 3
```

Commenting out lines in KJU script use "#", as above.

Constant Variables:
-------------------

All constant variables are prefixed with ```KJU declares```,
also the variable names start with a capital letter, like in ruby.

```ruby
KJU declares Constant is now "a Constant!!"
```

String functions:
-----------------

Printing:

Printing is as simple as print(), though this will probably be changed later on.

an example is: 

```python
print("Hello World!")
```

Converting anything to string:

Here we make use of something from python with ```str() ```

```python
str(1) #=> "1"

#  We can also use floats and long numbers

str(2.4) #=> "2.4"
```
Substring:

We can use java's substring to choose items from a string:

```python
KJU says string is now "hello"
string.substring(0,1) #=> "h"
```

Regex:

Currently KJUScript has a half assed implementation of regex, you will only return the first grouping.

Though you can do something like this:

```python

re(regex, string_to_search)

```

User Input:

Currently, user input is done like so with the "groriusOpinion" keyword:

```

KJU says x is now groriusOpinion("Please enter a number to be displayed on the screen: ")

print(x)

```

Functions:
----------

```ruby 
KJU commands x?(arg1, arg2) to be:
    arg1 + arg2
  shamefur end #End keyword
  
x?(1,2) #=> 3

x?("hello ","world!") #=> "hello world!"

```

If the function has no arguments, you may exclude parentheses all-together.

like so:

```ruby
KJU commands hello! to be:
    "Hello World!"
shamefur end # This is the "end" keyword

print(hello!) #=> "Hello World!"
```

In KJU Script, the last evaluated line in a function gets returned!


If/Else:
--------

If else blocks are done with the keywords ```when KJU senses``` for if, and ```when KJU is tired of sensing```
for else. All blocks must end with the ```shamefur end``` keywords

Here is an example:

```python
KJU says number is now 3

when KJU senses (number > 1)
    print(number)

when KJU is tired of sensing:
    print(number + " was not greater than one!")
    
shamefur end

```

Objects:
--------

OOP is implemented on KJUScript, it is modeled after ruby/python. Here is an example:

```ruby

The Grorius Reader Language < Object #Extends to object class

	KJU commands initialize(name) to be:
		# Instance variable start with an @
		KJU says @name is now name
	shamefur end

	KJU commands name to be:
		@name #remember that the last evaluated expression gets returned
	shamefur end

shamefur end

KJU says language is now Language.new("KJUScript") # Create the object

print(language.name)
# => KJUScript
print(language.class.name)
# => Language
print(Language.superclass.name)
# => Object
```

Try/Catch Block:
----------------

Try catch blocks use "KJUs space program attempt:" for try, and "IMPOSSIBRU!!" for except.

Here is an example of try/catch:

```python
KJUs space program attempt:
    undefinedmethod!
IMPOSSIBRU!! MethodNotFound : e
    print(e.class.name) #=> MethodNotFound
shamefur end
```

Other random goodies:
---------------------

Here are some random things i threw in

HTTP request:

```python

KJU says response is now httpforKJU("http://www.google.com")

print(response)

```



Is that it?
===========

No, there is a ton more to be doccumented (like full OOP implementation, and how to use it), just a bit too lazy to write it out right now.
