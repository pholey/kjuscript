package KJUScript.lang;

import java.util.HashMap;

/**
  Any object, instance of a class, inside the runtime.
  Objects store a class and instance variables.
*/
public class KJUScriptObject {
  private KJUScriptClass yourLangClass;
  private HashMap<String, KJUScriptObject> instanceVariables;
  
  /**
    Creates an instance of class yourLangClass.
  */
  public KJUScriptObject(KJUScriptClass yourLangClass) {
    this.yourLangClass = yourLangClass;
    this.instanceVariables = new HashMap<String, KJUScriptObject>();
  }
  
  public KJUScriptObject(String className) {
    this(KJUScriptRuntime.getRootClass(className));
  }
  
  public KJUScriptObject() {
    this(KJUScriptRuntime.getObjectClass());
  }
  
  public KJUScriptClass getKJUScriptClass() {
    return yourLangClass;
  }
  
  public void setKJUScriptClass(KJUScriptClass klass) {
    yourLangClass = klass;
  }
  
  public KJUScriptObject getInstanceVariable(String name) {
    if (hasInstanceVariable(name))
      return instanceVariables.get(name);
    return KJUScriptRuntime.getNil();
  }

  public boolean hasInstanceVariable(String name) {
    return instanceVariables.containsKey(name);
  }
  
  public void setInstanceVariable(String name, KJUScriptObject value) {
    instanceVariables.put(name, value);
  }
  
  /**
    Call a method on the object.
  */
  public KJUScriptObject call(String method, KJUScriptObject arguments[]) throws KJUScriptException {
    return yourLangClass.lookup(method).call(this, arguments);
  }

  public KJUScriptObject call(String method) throws KJUScriptException {
    return call(method, new KJUScriptObject[0]);
  }
  
  /**
    Only false and nil are not true.
  */
  public boolean isTrue() {
    return !isFalse();
  }
  
  /**
    Only false and nil are false. This is overridden in ValueObject.
  */
  public boolean isFalse() {
    return false;
  }

  /**
    Only nil is nil. This is overridden in ValueObject.
  */
  public boolean isNil() {
    return false;
  }
  
  /**
    Convert to a Java object. This is overridden in ValueObject.
  */
  public Object toJavaObject() {
    return this;
  }
  
  public <T> T as(Class<T> clazz) throws TypeError {
    if (clazz.isInstance(this)){
      return clazz.cast(this);
    }
    throw new TypeError(clazz.getName(), this);
  }
  
  public String asString() throws TypeError {
    return as(ValueObject.class).getValueAs(String.class);
  }

  public Integer asInteger() throws TypeError {
    return as(ValueObject.class).getValueAs(Integer.class);
  }

  public Float asFloat() throws TypeError {
    return as(ValueObject.class).getValueAs(Float.class);
  }
  public Long asLong() throws TypeError {
    return as(ValueObject.class).getValueAs(Long.class);
  }
}
