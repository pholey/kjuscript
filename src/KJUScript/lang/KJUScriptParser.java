// $ANTLR 3.x KJUScriptParser.g 2013-12-02 19:44:58

  package KJUScript.lang;
  
  import KJUScript.lang.nodes.*;
  import java.util.ArrayList;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class KJUScriptParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ASPREF", "ASSIGN", "AT", 
		"CATCH", "CLASS", "CLOSE_PARENT", "COLON", "COMMA", "COMMENT", "CONSTANT", 
		"DEF", "DIGIT", "DIV", "DOT", "ELSE", "END", "EQ", "FALSE", "FLOAT", "GE", 
		"GT", "ID_CHAR", "IF", "INTEGER", "LAM", "LE", "LETTER", "LOWER", "LT", 
		"MINUS", "MOD", "MUL", "NAME", "NEWLINE", "NIL", "NOT", "NUMBER", "OPEN_PARENT", 
		"OR", "PLUS", "SELF", "SEMICOLON", "SPACE", "STRING", "TOBE", "TRUE", 
		"TRY", "UPPER", "WHILE", "WHITESPACE"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ASPREF=5;
	public static final int ASSIGN=6;
	public static final int AT=7;
	public static final int CATCH=8;
	public static final int CLASS=9;
	public static final int CLOSE_PARENT=10;
	public static final int COLON=11;
	public static final int COMMA=12;
	public static final int COMMENT=13;
	public static final int CONSTANT=14;
	public static final int DEF=15;
	public static final int DIGIT=16;
	public static final int DIV=17;
	public static final int DOT=18;
	public static final int ELSE=19;
	public static final int END=20;
	public static final int EQ=21;
	public static final int FALSE=22;
	public static final int FLOAT=23;
	public static final int GE=24;
	public static final int GT=25;
	public static final int ID_CHAR=26;
	public static final int IF=27;
	public static final int INTEGER=28;
	public static final int LAM=29;
	public static final int LE=30;
	public static final int LETTER=31;
	public static final int LOWER=32;
	public static final int LT=33;
	public static final int MINUS=34;
	public static final int MOD=35;
	public static final int MUL=36;
	public static final int NAME=37;
	public static final int NEWLINE=38;
	public static final int NIL=39;
	public static final int NOT=40;
	public static final int NUMBER=41;
	public static final int OPEN_PARENT=42;
	public static final int OR=43;
	public static final int PLUS=44;
	public static final int SELF=45;
	public static final int SEMICOLON=46;
	public static final int SPACE=47;
	public static final int STRING=48;
	public static final int TOBE=49;
	public static final int TRUE=50;
	public static final int TRY=51;
	public static final int UPPER=52;
	public static final int WHILE=53;
	public static final int WHITESPACE=54;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public KJUScriptParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public KJUScriptParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return KJUScriptParser.tokenNames; }
	@Override public String getGrammarFileName() { return "KJUScriptParser.g"; }


	  public Node parse() throws RecognitionException {
	    root_return result = root();
	    if (result == null) return null;
	    return result.nodes;
	  }
	  
	  // Override to throw exceptions on parse error.
	  @Override
	  public void reportError(RecognitionException e) {
	    throw new RuntimeException(e);
	  }


	public static class root_return extends ParserRuleReturnScope {
		public Nodes nodes;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "root"
	// KJUScriptParser.g:52:1: root returns [Nodes nodes] : ( terminator )? ( expressions )? EOF !;
	public final KJUScriptParser.root_return root() throws RecognitionException {
		KJUScriptParser.root_return retval = new KJUScriptParser.root_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EOF3=null;
		ParserRuleReturnScope terminator1 =null;
		ParserRuleReturnScope expressions2 =null;

		Object EOF3_tree=null;

		try {
			// KJUScriptParser.g:52:27: ( ( terminator )? ( expressions )? EOF !)
			// KJUScriptParser.g:53:5: ( terminator )? ( expressions )? EOF !
			{
			root_0 = (Object)adaptor.nil();


			// KJUScriptParser.g:53:5: ( terminator )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==NEWLINE||LA1_0==SEMICOLON) ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// KJUScriptParser.g:53:5: terminator
					{
					pushFollow(FOLLOW_terminator_in_root77);
					terminator1=terminator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator1.getTree());

					}
					break;

			}

			// KJUScriptParser.g:53:17: ( expressions )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0==ASPREF||LA2_0==AT||LA2_0==CLASS||(LA2_0 >= CONSTANT && LA2_0 <= DEF)||(LA2_0 >= FALSE && LA2_0 <= FLOAT)||(LA2_0 >= IF && LA2_0 <= INTEGER)||LA2_0==NAME||(LA2_0 >= NIL && LA2_0 <= NOT)||LA2_0==OPEN_PARENT||LA2_0==SELF||LA2_0==STRING||(LA2_0 >= TRUE && LA2_0 <= TRY)||LA2_0==WHILE) ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// KJUScriptParser.g:53:17: expressions
					{
					pushFollow(FOLLOW_expressions_in_root80);
					expressions2=expressions();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expressions2.getTree());

					}
					break;

			}

			EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_root83); if (state.failed) return retval;
			if ( state.backtracking==0 ) { retval.nodes = (expressions2!=null?((KJUScriptParser.expressions_return)expressions2).nodes:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "root"


	public static class expressions_return extends ParserRuleReturnScope {
		public Nodes nodes;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expressions"
	// KJUScriptParser.g:57:1: expressions returns [Nodes nodes] :head= expression ( terminator tail= expression )* ( terminator )? ;
	public final KJUScriptParser.expressions_return expressions() throws RecognitionException {
		KJUScriptParser.expressions_return retval = new KJUScriptParser.expressions_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope head =null;
		ParserRuleReturnScope tail =null;
		ParserRuleReturnScope terminator4 =null;
		ParserRuleReturnScope terminator5 =null;


		try {
			// KJUScriptParser.g:57:34: (head= expression ( terminator tail= expression )* ( terminator )? )
			// KJUScriptParser.g:58:23: head= expression ( terminator tail= expression )* ( terminator )?
			{
			root_0 = (Object)adaptor.nil();


			if ( state.backtracking==0 ) { retval.nodes = new Nodes(); }
			pushFollow(FOLLOW_expression_in_expressions131);
			head=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, head.getTree());

			if ( state.backtracking==0 ) { retval.nodes.add((head!=null?((KJUScriptParser.expression_return)head).node:null)); }
			// KJUScriptParser.g:60:5: ( terminator tail= expression )*
			loop3:
			while (true) {
				int alt3=2;
				alt3 = dfa3.predict(input);
				switch (alt3) {
				case 1 :
					// KJUScriptParser.g:60:6: terminator tail= expression
					{
					pushFollow(FOLLOW_terminator_in_expressions142);
					terminator4=terminator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator4.getTree());

					pushFollow(FOLLOW_expression_in_expressions151);
					tail=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, tail.getTree());

					if ( state.backtracking==0 ) { retval.nodes.add((tail!=null?((KJUScriptParser.expression_return)tail).node:null)); }
					}
					break;

				default :
					break loop3;
				}
			}

			// KJUScriptParser.g:63:5: ( terminator )?
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==NEWLINE||LA4_0==SEMICOLON) ) {
				alt4=1;
			}
			switch (alt4) {
				case 1 :
					// KJUScriptParser.g:63:5: terminator
					{
					pushFollow(FOLLOW_terminator_in_expressions167);
					terminator5=terminator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator5.getTree());

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expressions"


	public static class expression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// KJUScriptParser.g:67:1: expression returns [Node node] : assignExpression ;
	public final KJUScriptParser.expression_return expression() throws RecognitionException {
		KJUScriptParser.expression_return retval = new KJUScriptParser.expression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope assignExpression6 =null;


		try {
			// KJUScriptParser.g:67:31: ( assignExpression )
			// KJUScriptParser.g:68:5: assignExpression
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_assignExpression_in_expression187);
			assignExpression6=assignExpression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, assignExpression6.getTree());

			if ( state.backtracking==0 ) { retval.node = (assignExpression6!=null?((KJUScriptParser.assignExpression_return)assignExpression6).node:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class terminator_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "terminator"
	// KJUScriptParser.g:72:1: terminator : ( NEWLINE | SEMICOLON )+ ;
	public final KJUScriptParser.terminator_return terminator() throws RecognitionException {
		KJUScriptParser.terminator_return retval = new KJUScriptParser.terminator_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set7=null;

		Object set7_tree=null;

		try {
			// KJUScriptParser.g:72:11: ( ( NEWLINE | SEMICOLON )+ )
			// KJUScriptParser.g:72:13: ( NEWLINE | SEMICOLON )+
			{
			root_0 = (Object)adaptor.nil();


			// KJUScriptParser.g:72:13: ( NEWLINE | SEMICOLON )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==NEWLINE||LA5_0==SEMICOLON) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// KJUScriptParser.g:
					{
					set7=input.LT(1);
					if ( input.LA(1)==NEWLINE||input.LA(1)==SEMICOLON ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(set7));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					if (state.backtracking>0) {state.failed=true; return retval;}
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "terminator"


	public static class assignExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assignExpression"
	// KJUScriptParser.g:78:1: assignExpression returns [Node node] : ( assign |e= orExpression );
	public final KJUScriptParser.assignExpression_return assignExpression() throws RecognitionException {
		KJUScriptParser.assignExpression_return retval = new KJUScriptParser.assignExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope e =null;
		ParserRuleReturnScope assign8 =null;


		try {
			// KJUScriptParser.g:78:37: ( assign |e= orExpression )
			int alt6=2;
			switch ( input.LA(1) ) {
			case NAME:
				{
				int LA6_1 = input.LA(2);
				if ( (LA6_1==ASSIGN) ) {
					alt6=1;
				}
				else if ( (LA6_1==EOF||LA6_1==AND||LA6_1==CATCH||LA6_1==CLOSE_PARENT||LA6_1==COMMA||(LA6_1 >= DIV && LA6_1 <= EQ)||(LA6_1 >= GE && LA6_1 <= GT)||LA6_1==LE||(LA6_1 >= LT && LA6_1 <= MUL)||LA6_1==NEWLINE||(LA6_1 >= OPEN_PARENT && LA6_1 <= PLUS)||LA6_1==SEMICOLON) ) {
					alt6=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case ASPREF:
				{
				alt6=1;
				}
				break;
			case CONSTANT:
				{
				int LA6_3 = input.LA(2);
				if ( (LA6_3==ASSIGN) ) {
					alt6=1;
				}
				else if ( (LA6_3==EOF||LA6_3==AND||LA6_3==CATCH||LA6_3==CLOSE_PARENT||LA6_3==COMMA||(LA6_3 >= DIV && LA6_3 <= EQ)||(LA6_3 >= GE && LA6_3 <= GT)||LA6_3==LE||(LA6_3 >= LT && LA6_3 <= MUL)||LA6_3==NEWLINE||(LA6_3 >= OR && LA6_3 <= PLUS)||LA6_3==SEMICOLON) ) {
					alt6=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case AT:
				{
				int LA6_4 = input.LA(2);
				if ( (LA6_4==NAME) ) {
					int LA6_6 = input.LA(3);
					if ( (LA6_6==ASSIGN) ) {
						alt6=1;
					}
					else if ( (LA6_6==EOF||LA6_6==AND||LA6_6==CATCH||LA6_6==CLOSE_PARENT||LA6_6==COMMA||(LA6_6 >= DIV && LA6_6 <= EQ)||(LA6_6 >= GE && LA6_6 <= GT)||LA6_6==LE||(LA6_6 >= LT && LA6_6 <= MUL)||LA6_6==NEWLINE||(LA6_6 >= OR && LA6_6 <= PLUS)||LA6_6==SEMICOLON) ) {
						alt6=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 6, 6, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case CLASS:
			case DEF:
			case FALSE:
			case FLOAT:
			case IF:
			case INTEGER:
			case NIL:
			case NOT:
			case OPEN_PARENT:
			case SELF:
			case STRING:
			case TRUE:
			case TRY:
			case WHILE:
				{
				alt6=2;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			switch (alt6) {
				case 1 :
					// KJUScriptParser.g:79:5: assign
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_assign_in_assignExpression231);
					assign8=assign();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, assign8.getTree());

					if ( state.backtracking==0 ) { retval.node = (assign8!=null?((KJUScriptParser.assign_return)assign8).node:null); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:80:5: e= orExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_orExpression_in_assignExpression256);
					e=orExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.orExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assignExpression"


	public static class orExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "orExpression"
	// KJUScriptParser.g:83:1: orExpression returns [Node node] : (receiver= andExpression OR arg= orExpression |e= andExpression );
	public final KJUScriptParser.orExpression_return orExpression() throws RecognitionException {
		KJUScriptParser.orExpression_return retval = new KJUScriptParser.orExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token OR9=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;
		ParserRuleReturnScope e =null;

		Object OR9_tree=null;

		try {
			// KJUScriptParser.g:83:33: (receiver= andExpression OR arg= orExpression |e= andExpression )
			int alt7=2;
			switch ( input.LA(1) ) {
			case NOT:
				{
				int LA7_1 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case STRING:
				{
				int LA7_2 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case INTEGER:
				{
				int LA7_3 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case FLOAT:
				{
				int LA7_4 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case NIL:
				{
				int LA7_5 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case TRUE:
				{
				int LA7_6 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case FALSE:
				{
				int LA7_7 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case CONSTANT:
				{
				int LA7_8 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case AT:
				{
				int LA7_9 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case SELF:
				{
				int LA7_10 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case NAME:
				{
				int LA7_11 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case DEF:
				{
				int LA7_12 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case CLASS:
				{
				int LA7_13 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case IF:
				{
				int LA7_14 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case WHILE:
				{
				int LA7_15 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case TRY:
				{
				int LA7_16 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			case OPEN_PARENT:
				{
				int LA7_17 = input.LA(2);
				if ( (synpred8_KJUScriptParser()) ) {
					alt7=1;
				}
				else if ( (true) ) {
					alt7=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}
			switch (alt7) {
				case 1 :
					// KJUScriptParser.g:84:5: receiver= andExpression OR arg= orExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_andExpression_in_orExpression285);
					receiver=andExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, receiver.getTree());

					OR9=(Token)match(input,OR,FOLLOW_OR_in_orExpression293); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					OR9_tree = (Object)adaptor.create(OR9);
					adaptor.addChild(root_0, OR9_tree);
					}

					pushFollow(FOLLOW_orExpression_in_orExpression297);
					arg=orExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arg.getTree());

					if ( state.backtracking==0 ) { retval.node = new OrNode((receiver!=null?((KJUScriptParser.andExpression_return)receiver).node:null), (arg!=null?((KJUScriptParser.orExpression_return)arg).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:86:5: e= andExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_andExpression_in_orExpression313);
					e=andExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.andExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "orExpression"


	public static class andExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "andExpression"
	// KJUScriptParser.g:89:1: andExpression returns [Node node] : (receiver= relationalExpression AND arg= andExpression |e= relationalExpression );
	public final KJUScriptParser.andExpression_return andExpression() throws RecognitionException {
		KJUScriptParser.andExpression_return retval = new KJUScriptParser.andExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token AND10=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;
		ParserRuleReturnScope e =null;

		Object AND10_tree=null;

		try {
			// KJUScriptParser.g:89:34: (receiver= relationalExpression AND arg= andExpression |e= relationalExpression )
			int alt8=2;
			switch ( input.LA(1) ) {
			case NOT:
				{
				int LA8_1 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case STRING:
				{
				int LA8_2 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case INTEGER:
				{
				int LA8_3 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case FLOAT:
				{
				int LA8_4 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case NIL:
				{
				int LA8_5 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case TRUE:
				{
				int LA8_6 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case FALSE:
				{
				int LA8_7 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case CONSTANT:
				{
				int LA8_8 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case AT:
				{
				int LA8_9 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case SELF:
				{
				int LA8_10 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case NAME:
				{
				int LA8_11 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case DEF:
				{
				int LA8_12 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case CLASS:
				{
				int LA8_13 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case IF:
				{
				int LA8_14 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case WHILE:
				{
				int LA8_15 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case TRY:
				{
				int LA8_16 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			case OPEN_PARENT:
				{
				int LA8_17 = input.LA(2);
				if ( (synpred9_KJUScriptParser()) ) {
					alt8=1;
				}
				else if ( (true) ) {
					alt8=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}
			switch (alt8) {
				case 1 :
					// KJUScriptParser.g:90:5: receiver= relationalExpression AND arg= andExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_relationalExpression_in_andExpression347);
					receiver=relationalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, receiver.getTree());

					AND10=(Token)match(input,AND,FOLLOW_AND_in_andExpression355); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					AND10_tree = (Object)adaptor.create(AND10);
					adaptor.addChild(root_0, AND10_tree);
					}

					pushFollow(FOLLOW_andExpression_in_andExpression359);
					arg=andExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arg.getTree());

					if ( state.backtracking==0 ) { retval.node = new AndNode((receiver!=null?((KJUScriptParser.relationalExpression_return)receiver).node:null), (arg!=null?((KJUScriptParser.andExpression_return)arg).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:92:5: e= relationalExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_relationalExpression_in_andExpression373);
					e=relationalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.relationalExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "andExpression"


	public static class relationalExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "relationalExpression"
	// KJUScriptParser.g:95:1: relationalExpression returns [Node node] : (receiver= additiveExpression op= ( EQ | LE | GE | LT | GT ) arg= relationalExpression |e= additiveExpression );
	public final KJUScriptParser.relationalExpression_return relationalExpression() throws RecognitionException {
		KJUScriptParser.relationalExpression_return retval = new KJUScriptParser.relationalExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token op=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;
		ParserRuleReturnScope e =null;

		Object op_tree=null;

		try {
			// KJUScriptParser.g:95:41: (receiver= additiveExpression op= ( EQ | LE | GE | LT | GT ) arg= relationalExpression |e= additiveExpression )
			int alt9=2;
			switch ( input.LA(1) ) {
			case NOT:
				{
				int LA9_1 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case STRING:
				{
				int LA9_2 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case INTEGER:
				{
				int LA9_3 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case FLOAT:
				{
				int LA9_4 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case NIL:
				{
				int LA9_5 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case TRUE:
				{
				int LA9_6 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case FALSE:
				{
				int LA9_7 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case CONSTANT:
				{
				int LA9_8 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case AT:
				{
				int LA9_9 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case SELF:
				{
				int LA9_10 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case NAME:
				{
				int LA9_11 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case DEF:
				{
				int LA9_12 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case CLASS:
				{
				int LA9_13 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case IF:
				{
				int LA9_14 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case WHILE:
				{
				int LA9_15 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case TRY:
				{
				int LA9_16 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			case OPEN_PARENT:
				{
				int LA9_17 = input.LA(2);
				if ( (synpred14_KJUScriptParser()) ) {
					alt9=1;
				}
				else if ( (true) ) {
					alt9=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}
			switch (alt9) {
				case 1 :
					// KJUScriptParser.g:96:5: receiver= additiveExpression op= ( EQ | LE | GE | LT | GT ) arg= relationalExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_additiveExpression_in_relationalExpression400);
					receiver=additiveExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, receiver.getTree());

					op=input.LT(1);
					if ( input.LA(1)==EQ||(input.LA(1) >= GE && input.LA(1) <= GT)||input.LA(1)==LE||input.LA(1)==LT ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(op));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_relationalExpression_in_relationalExpression430);
					arg=relationalExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arg.getTree());

					if ( state.backtracking==0 ) { retval.node = new CallNode((op!=null?op.getText():null), (receiver!=null?((KJUScriptParser.additiveExpression_return)receiver).node:null), (arg!=null?((KJUScriptParser.relationalExpression_return)arg).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:99:5: e= additiveExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_additiveExpression_in_relationalExpression441);
					e=additiveExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.additiveExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "relationalExpression"


	public static class additiveExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "additiveExpression"
	// KJUScriptParser.g:102:1: additiveExpression returns [Node node] : (receiver= multiplicativeExpression op= ( PLUS | MINUS ) arg= additiveExpression |e= multiplicativeExpression );
	public final KJUScriptParser.additiveExpression_return additiveExpression() throws RecognitionException {
		KJUScriptParser.additiveExpression_return retval = new KJUScriptParser.additiveExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token op=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;
		ParserRuleReturnScope e =null;

		Object op_tree=null;

		try {
			// KJUScriptParser.g:102:39: (receiver= multiplicativeExpression op= ( PLUS | MINUS ) arg= additiveExpression |e= multiplicativeExpression )
			int alt10=2;
			switch ( input.LA(1) ) {
			case NOT:
				{
				int LA10_1 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case STRING:
				{
				int LA10_2 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case INTEGER:
				{
				int LA10_3 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case FLOAT:
				{
				int LA10_4 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case NIL:
				{
				int LA10_5 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case TRUE:
				{
				int LA10_6 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case FALSE:
				{
				int LA10_7 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case CONSTANT:
				{
				int LA10_8 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case AT:
				{
				int LA10_9 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case SELF:
				{
				int LA10_10 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case NAME:
				{
				int LA10_11 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case DEF:
				{
				int LA10_12 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case CLASS:
				{
				int LA10_13 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case IF:
				{
				int LA10_14 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case WHILE:
				{
				int LA10_15 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case TRY:
				{
				int LA10_16 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			case OPEN_PARENT:
				{
				int LA10_17 = input.LA(2);
				if ( (synpred16_KJUScriptParser()) ) {
					alt10=1;
				}
				else if ( (true) ) {
					alt10=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}
			switch (alt10) {
				case 1 :
					// KJUScriptParser.g:103:5: receiver= multiplicativeExpression op= ( PLUS | MINUS ) arg= additiveExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression470);
					receiver=multiplicativeExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, receiver.getTree());

					op=input.LT(1);
					if ( input.LA(1)==MINUS||input.LA(1)==PLUS ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(op));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_additiveExpression_in_additiveExpression488);
					arg=additiveExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arg.getTree());

					if ( state.backtracking==0 ) { retval.node = new CallNode((op!=null?op.getText():null), (receiver!=null?((KJUScriptParser.multiplicativeExpression_return)receiver).node:null), (arg!=null?((KJUScriptParser.additiveExpression_return)arg).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:105:5: e= multiplicativeExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression499);
					e=multiplicativeExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.multiplicativeExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "additiveExpression"


	public static class multiplicativeExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "multiplicativeExpression"
	// KJUScriptParser.g:108:1: multiplicativeExpression returns [Node node] : (receiver= unaryExpression op= ( MUL | DIV | MOD ) arg= multiplicativeExpression |e= unaryExpression );
	public final KJUScriptParser.multiplicativeExpression_return multiplicativeExpression() throws RecognitionException {
		KJUScriptParser.multiplicativeExpression_return retval = new KJUScriptParser.multiplicativeExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token op=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;
		ParserRuleReturnScope e =null;

		Object op_tree=null;

		try {
			// KJUScriptParser.g:108:45: (receiver= unaryExpression op= ( MUL | DIV | MOD ) arg= multiplicativeExpression |e= unaryExpression )
			int alt11=2;
			switch ( input.LA(1) ) {
			case NOT:
				{
				int LA11_1 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case STRING:
				{
				int LA11_2 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case INTEGER:
				{
				int LA11_3 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case FLOAT:
				{
				int LA11_4 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case NIL:
				{
				int LA11_5 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case TRUE:
				{
				int LA11_6 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case FALSE:
				{
				int LA11_7 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case CONSTANT:
				{
				int LA11_8 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case AT:
				{
				int LA11_9 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case SELF:
				{
				int LA11_10 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case NAME:
				{
				int LA11_11 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case DEF:
				{
				int LA11_12 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case CLASS:
				{
				int LA11_13 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case IF:
				{
				int LA11_14 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case WHILE:
				{
				int LA11_15 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case TRY:
				{
				int LA11_16 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			case OPEN_PARENT:
				{
				int LA11_17 = input.LA(2);
				if ( (synpred19_KJUScriptParser()) ) {
					alt11=1;
				}
				else if ( (true) ) {
					alt11=2;
				}

				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}
			switch (alt11) {
				case 1 :
					// KJUScriptParser.g:109:5: receiver= unaryExpression op= ( MUL | DIV | MOD ) arg= multiplicativeExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression536);
					receiver=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, receiver.getTree());

					op=input.LT(1);
					if ( input.LA(1)==DIV||(input.LA(1) >= MOD && input.LA(1) <= MUL) ) {
						input.consume();
						if ( state.backtracking==0 ) adaptor.addChild(root_0, (Object)adaptor.create(op));
						state.errorRecovery=false;
						state.failed=false;
					}
					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					pushFollow(FOLLOW_multiplicativeExpression_in_multiplicativeExpression556);
					arg=multiplicativeExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arg.getTree());

					if ( state.backtracking==0 ) { retval.node = new CallNode((op!=null?op.getText():null), (receiver!=null?((KJUScriptParser.unaryExpression_return)receiver).node:null), (arg!=null?((KJUScriptParser.multiplicativeExpression_return)arg).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:111:5: e= unaryExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression567);
					e=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.unaryExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "multiplicativeExpression"


	public static class unaryExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "unaryExpression"
	// KJUScriptParser.g:114:1: unaryExpression returns [Node node] : ( NOT receiver= unaryExpression |e= primaryExpression );
	public final KJUScriptParser.unaryExpression_return unaryExpression() throws RecognitionException {
		KJUScriptParser.unaryExpression_return retval = new KJUScriptParser.unaryExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NOT11=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope e =null;

		Object NOT11_tree=null;

		try {
			// KJUScriptParser.g:114:36: ( NOT receiver= unaryExpression |e= primaryExpression )
			int alt12=2;
			int LA12_0 = input.LA(1);
			if ( (LA12_0==NOT) ) {
				alt12=1;
			}
			else if ( (LA12_0==AT||LA12_0==CLASS||(LA12_0 >= CONSTANT && LA12_0 <= DEF)||(LA12_0 >= FALSE && LA12_0 <= FLOAT)||(LA12_0 >= IF && LA12_0 <= INTEGER)||LA12_0==NAME||LA12_0==NIL||LA12_0==OPEN_PARENT||LA12_0==SELF||LA12_0==STRING||(LA12_0 >= TRUE && LA12_0 <= TRY)||LA12_0==WHILE) ) {
				alt12=2;
			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}

			switch (alt12) {
				case 1 :
					// KJUScriptParser.g:115:5: NOT receiver= unaryExpression
					{
					root_0 = (Object)adaptor.nil();


					NOT11=(Token)match(input,NOT,FOLLOW_NOT_in_unaryExpression618); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NOT11_tree = (Object)adaptor.create(NOT11);
					adaptor.addChild(root_0, NOT11_tree);
					}

					pushFollow(FOLLOW_unaryExpression_in_unaryExpression622);
					receiver=unaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, receiver.getTree());

					if ( state.backtracking==0 ) { retval.node = new NotNode((receiver!=null?((KJUScriptParser.unaryExpression_return)receiver).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:116:5: e= primaryExpression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_primaryExpression_in_unaryExpression638);
					e=primaryExpression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, e.getTree());

					if ( state.backtracking==0 ) { retval.node = (e!=null?((KJUScriptParser.primaryExpression_return)e).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "unaryExpression"


	public static class primaryExpression_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "primaryExpression"
	// KJUScriptParser.g:120:1: primaryExpression returns [Node node] : ( literal | call | methodDefinition | classDefinition | ifBlock | whileBlock | tryBlock | OPEN_PARENT expression CLOSE_PARENT );
	public final KJUScriptParser.primaryExpression_return primaryExpression() throws RecognitionException {
		KJUScriptParser.primaryExpression_return retval = new KJUScriptParser.primaryExpression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token OPEN_PARENT19=null;
		Token CLOSE_PARENT21=null;
		ParserRuleReturnScope literal12 =null;
		ParserRuleReturnScope call13 =null;
		ParserRuleReturnScope methodDefinition14 =null;
		ParserRuleReturnScope classDefinition15 =null;
		ParserRuleReturnScope ifBlock16 =null;
		ParserRuleReturnScope whileBlock17 =null;
		ParserRuleReturnScope tryBlock18 =null;
		ParserRuleReturnScope expression20 =null;

		Object OPEN_PARENT19_tree=null;
		Object CLOSE_PARENT21_tree=null;

		try {
			// KJUScriptParser.g:120:38: ( literal | call | methodDefinition | classDefinition | ifBlock | whileBlock | tryBlock | OPEN_PARENT expression CLOSE_PARENT )
			int alt13=8;
			switch ( input.LA(1) ) {
			case STRING:
				{
				int LA13_1 = input.LA(2);
				if ( (LA13_1==EOF||LA13_1==AND||LA13_1==CATCH||LA13_1==CLOSE_PARENT||LA13_1==COMMA||LA13_1==DIV||(LA13_1 >= ELSE && LA13_1 <= EQ)||(LA13_1 >= GE && LA13_1 <= GT)||LA13_1==LE||(LA13_1 >= LT && LA13_1 <= MUL)||LA13_1==NEWLINE||(LA13_1 >= OR && LA13_1 <= PLUS)||LA13_1==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_1==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case INTEGER:
				{
				int LA13_2 = input.LA(2);
				if ( (LA13_2==EOF||LA13_2==AND||LA13_2==CATCH||LA13_2==CLOSE_PARENT||LA13_2==COMMA||LA13_2==DIV||(LA13_2 >= ELSE && LA13_2 <= EQ)||(LA13_2 >= GE && LA13_2 <= GT)||LA13_2==LE||(LA13_2 >= LT && LA13_2 <= MUL)||LA13_2==NEWLINE||(LA13_2 >= OR && LA13_2 <= PLUS)||LA13_2==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_2==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case FLOAT:
				{
				int LA13_3 = input.LA(2);
				if ( (LA13_3==EOF||LA13_3==AND||LA13_3==CATCH||LA13_3==CLOSE_PARENT||LA13_3==COMMA||LA13_3==DIV||(LA13_3 >= ELSE && LA13_3 <= EQ)||(LA13_3 >= GE && LA13_3 <= GT)||LA13_3==LE||(LA13_3 >= LT && LA13_3 <= MUL)||LA13_3==NEWLINE||(LA13_3 >= OR && LA13_3 <= PLUS)||LA13_3==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_3==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NIL:
				{
				int LA13_4 = input.LA(2);
				if ( (LA13_4==EOF||LA13_4==AND||LA13_4==CATCH||LA13_4==CLOSE_PARENT||LA13_4==COMMA||LA13_4==DIV||(LA13_4 >= ELSE && LA13_4 <= EQ)||(LA13_4 >= GE && LA13_4 <= GT)||LA13_4==LE||(LA13_4 >= LT && LA13_4 <= MUL)||LA13_4==NEWLINE||(LA13_4 >= OR && LA13_4 <= PLUS)||LA13_4==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_4==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case TRUE:
				{
				int LA13_5 = input.LA(2);
				if ( (LA13_5==EOF||LA13_5==AND||LA13_5==CATCH||LA13_5==CLOSE_PARENT||LA13_5==COMMA||LA13_5==DIV||(LA13_5 >= ELSE && LA13_5 <= EQ)||(LA13_5 >= GE && LA13_5 <= GT)||LA13_5==LE||(LA13_5 >= LT && LA13_5 <= MUL)||LA13_5==NEWLINE||(LA13_5 >= OR && LA13_5 <= PLUS)||LA13_5==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_5==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case FALSE:
				{
				int LA13_6 = input.LA(2);
				if ( (LA13_6==EOF||LA13_6==AND||LA13_6==CATCH||LA13_6==CLOSE_PARENT||LA13_6==COMMA||LA13_6==DIV||(LA13_6 >= ELSE && LA13_6 <= EQ)||(LA13_6 >= GE && LA13_6 <= GT)||LA13_6==LE||(LA13_6 >= LT && LA13_6 <= MUL)||LA13_6==NEWLINE||(LA13_6 >= OR && LA13_6 <= PLUS)||LA13_6==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_6==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case CONSTANT:
				{
				int LA13_7 = input.LA(2);
				if ( (LA13_7==EOF||LA13_7==AND||LA13_7==CATCH||LA13_7==CLOSE_PARENT||LA13_7==COMMA||LA13_7==DIV||(LA13_7 >= ELSE && LA13_7 <= EQ)||(LA13_7 >= GE && LA13_7 <= GT)||LA13_7==LE||(LA13_7 >= LT && LA13_7 <= MUL)||LA13_7==NEWLINE||(LA13_7 >= OR && LA13_7 <= PLUS)||LA13_7==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_7==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 7, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case AT:
				{
				int LA13_8 = input.LA(2);
				if ( (LA13_8==NAME) ) {
					int LA13_18 = input.LA(3);
					if ( (LA13_18==EOF||LA13_18==AND||LA13_18==CATCH||LA13_18==CLOSE_PARENT||LA13_18==COMMA||LA13_18==DIV||(LA13_18 >= ELSE && LA13_18 <= EQ)||(LA13_18 >= GE && LA13_18 <= GT)||LA13_18==LE||(LA13_18 >= LT && LA13_18 <= MUL)||LA13_18==NEWLINE||(LA13_18 >= OR && LA13_18 <= PLUS)||LA13_18==SEMICOLON) ) {
						alt13=1;
					}
					else if ( (LA13_18==DOT) ) {
						alt13=2;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 13, 18, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 8, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case SELF:
				{
				int LA13_9 = input.LA(2);
				if ( (LA13_9==EOF||LA13_9==AND||LA13_9==CATCH||LA13_9==CLOSE_PARENT||LA13_9==COMMA||LA13_9==DIV||(LA13_9 >= ELSE && LA13_9 <= EQ)||(LA13_9 >= GE && LA13_9 <= GT)||LA13_9==LE||(LA13_9 >= LT && LA13_9 <= MUL)||LA13_9==NEWLINE||(LA13_9 >= OR && LA13_9 <= PLUS)||LA13_9==SEMICOLON) ) {
					alt13=1;
				}
				else if ( (LA13_9==DOT) ) {
					alt13=2;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 9, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NAME:
				{
				alt13=2;
				}
				break;
			case DEF:
				{
				alt13=3;
				}
				break;
			case CLASS:
				{
				alt13=4;
				}
				break;
			case IF:
				{
				alt13=5;
				}
				break;
			case WHILE:
				{
				alt13=6;
				}
				break;
			case TRY:
				{
				alt13=7;
				}
				break;
			case OPEN_PARENT:
				{
				alt13=8;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// KJUScriptParser.g:121:5: literal
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_literal_in_primaryExpression674);
					literal12=literal();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, literal12.getTree());

					if ( state.backtracking==0 ) { retval.node = (literal12!=null?((KJUScriptParser.literal_return)literal12).node:null); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:122:5: call
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_call_in_primaryExpression692);
					call13=call();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, call13.getTree());

					if ( state.backtracking==0 ) { retval.node = (call13!=null?((KJUScriptParser.call_return)call13).node:null); }
					}
					break;
				case 3 :
					// KJUScriptParser.g:123:5: methodDefinition
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_methodDefinition_in_primaryExpression713);
					methodDefinition14=methodDefinition();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, methodDefinition14.getTree());

					if ( state.backtracking==0 ) { retval.node = (methodDefinition14!=null?((KJUScriptParser.methodDefinition_return)methodDefinition14).node:null); }
					}
					break;
				case 4 :
					// KJUScriptParser.g:124:5: classDefinition
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_classDefinition_in_primaryExpression722);
					classDefinition15=classDefinition();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, classDefinition15.getTree());

					if ( state.backtracking==0 ) { retval.node = (classDefinition15!=null?((KJUScriptParser.classDefinition_return)classDefinition15).node:null); }
					}
					break;
				case 5 :
					// KJUScriptParser.g:125:5: ifBlock
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_ifBlock_in_primaryExpression732);
					ifBlock16=ifBlock();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, ifBlock16.getTree());

					if ( state.backtracking==0 ) { retval.node = (ifBlock16!=null?((KJUScriptParser.ifBlock_return)ifBlock16).node:null); }
					}
					break;
				case 6 :
					// KJUScriptParser.g:126:5: whileBlock
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_whileBlock_in_primaryExpression750);
					whileBlock17=whileBlock();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, whileBlock17.getTree());

					if ( state.backtracking==0 ) { retval.node = (whileBlock17!=null?((KJUScriptParser.whileBlock_return)whileBlock17).node:null); }
					}
					break;
				case 7 :
					// KJUScriptParser.g:127:5: tryBlock
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_tryBlock_in_primaryExpression765);
					tryBlock18=tryBlock();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, tryBlock18.getTree());

					if ( state.backtracking==0 ) { retval.node = (tryBlock18!=null?((KJUScriptParser.tryBlock_return)tryBlock18).node:null); }
					}
					break;
				case 8 :
					// KJUScriptParser.g:128:5: OPEN_PARENT expression CLOSE_PARENT
					{
					root_0 = (Object)adaptor.nil();


					OPEN_PARENT19=(Token)match(input,OPEN_PARENT,FOLLOW_OPEN_PARENT_in_primaryExpression782); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					OPEN_PARENT19_tree = (Object)adaptor.create(OPEN_PARENT19);
					adaptor.addChild(root_0, OPEN_PARENT19_tree);
					}

					pushFollow(FOLLOW_expression_in_primaryExpression790);
					expression20=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression20.getTree());

					CLOSE_PARENT21=(Token)match(input,CLOSE_PARENT,FOLLOW_CLOSE_PARENT_in_primaryExpression796); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CLOSE_PARENT21_tree = (Object)adaptor.create(CLOSE_PARENT21);
					adaptor.addChild(root_0, CLOSE_PARENT21_tree);
					}

					if ( state.backtracking==0 ) { retval.node = (expression20!=null?((KJUScriptParser.expression_return)expression20).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "primaryExpression"


	public static class literal_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "literal"
	// KJUScriptParser.g:134:1: literal returns [Node node] : ( STRING | INTEGER | FLOAT | NIL | TRUE | FALSE | constant | instanceVariable | self );
	public final KJUScriptParser.literal_return literal() throws RecognitionException {
		KJUScriptParser.literal_return retval = new KJUScriptParser.literal_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token STRING22=null;
		Token INTEGER23=null;
		Token FLOAT24=null;
		Token NIL25=null;
		Token TRUE26=null;
		Token FALSE27=null;
		ParserRuleReturnScope constant28 =null;
		ParserRuleReturnScope instanceVariable29 =null;
		ParserRuleReturnScope self30 =null;

		Object STRING22_tree=null;
		Object INTEGER23_tree=null;
		Object FLOAT24_tree=null;
		Object NIL25_tree=null;
		Object TRUE26_tree=null;
		Object FALSE27_tree=null;

		try {
			// KJUScriptParser.g:134:28: ( STRING | INTEGER | FLOAT | NIL | TRUE | FALSE | constant | instanceVariable | self )
			int alt14=9;
			switch ( input.LA(1) ) {
			case STRING:
				{
				alt14=1;
				}
				break;
			case INTEGER:
				{
				alt14=2;
				}
				break;
			case FLOAT:
				{
				alt14=3;
				}
				break;
			case NIL:
				{
				alt14=4;
				}
				break;
			case TRUE:
				{
				alt14=5;
				}
				break;
			case FALSE:
				{
				alt14=6;
				}
				break;
			case CONSTANT:
				{
				alt14=7;
				}
				break;
			case AT:
				{
				alt14=8;
				}
				break;
			case SELF:
				{
				alt14=9;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}
			switch (alt14) {
				case 1 :
					// KJUScriptParser.g:135:5: STRING
					{
					root_0 = (Object)adaptor.nil();


					STRING22=(Token)match(input,STRING,FOLLOW_STRING_in_literal822); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					STRING22_tree = (Object)adaptor.create(STRING22);
					adaptor.addChild(root_0, STRING22_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new LiteralNode(new ValueObject((STRING22!=null?STRING22.getText():null).substring(1, (STRING22!=null?STRING22.getText():null).length() - 1))); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:136:5: INTEGER
					{
					root_0 = (Object)adaptor.nil();


					INTEGER23=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_literal841); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					INTEGER23_tree = (Object)adaptor.create(INTEGER23);
					adaptor.addChild(root_0, INTEGER23_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new LiteralNode(new ValueObject(new Integer((INTEGER23!=null?INTEGER23.getText():null)))); }
					}
					break;
				case 3 :
					// KJUScriptParser.g:137:5: FLOAT
					{
					root_0 = (Object)adaptor.nil();


					FLOAT24=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_literal859); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					FLOAT24_tree = (Object)adaptor.create(FLOAT24);
					adaptor.addChild(root_0, FLOAT24_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new LiteralNode(new ValueObject(new Float((FLOAT24!=null?FLOAT24.getText():null)))); }
					}
					break;
				case 4 :
					// KJUScriptParser.g:138:5: NIL
					{
					root_0 = (Object)adaptor.nil();


					NIL25=(Token)match(input,NIL,FOLLOW_NIL_in_literal879); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NIL25_tree = (Object)adaptor.create(NIL25);
					adaptor.addChild(root_0, NIL25_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new LiteralNode(KJUScriptRuntime.getNil()); }
					}
					break;
				case 5 :
					// KJUScriptParser.g:139:5: TRUE
					{
					root_0 = (Object)adaptor.nil();


					TRUE26=(Token)match(input,TRUE,FOLLOW_TRUE_in_literal901); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					TRUE26_tree = (Object)adaptor.create(TRUE26);
					adaptor.addChild(root_0, TRUE26_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new LiteralNode(KJUScriptRuntime.getTrue()); }
					}
					break;
				case 6 :
					// KJUScriptParser.g:140:5: FALSE
					{
					root_0 = (Object)adaptor.nil();


					FALSE27=(Token)match(input,FALSE,FOLLOW_FALSE_in_literal922); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					FALSE27_tree = (Object)adaptor.create(FALSE27);
					adaptor.addChild(root_0, FALSE27_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new LiteralNode(KJUScriptRuntime.getFalse()); }
					}
					break;
				case 7 :
					// KJUScriptParser.g:141:5: constant
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_constant_in_literal942);
					constant28=constant();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, constant28.getTree());

					if ( state.backtracking==0 ) { retval.node = (constant28!=null?((KJUScriptParser.constant_return)constant28).node:null); }
					}
					break;
				case 8 :
					// KJUScriptParser.g:142:5: instanceVariable
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_instanceVariable_in_literal959);
					instanceVariable29=instanceVariable();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, instanceVariable29.getTree());

					if ( state.backtracking==0 ) { retval.node = (instanceVariable29!=null?((KJUScriptParser.instanceVariable_return)instanceVariable29).node:null); }
					}
					break;
				case 9 :
					// KJUScriptParser.g:143:5: self
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_self_in_literal968);
					self30=self();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, self30.getTree());

					if ( state.backtracking==0 ) { retval.node = (self30!=null?((KJUScriptParser.self_return)self30).node:null); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "literal"


	public static class self_return extends ParserRuleReturnScope {
		public SelfNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "self"
	// KJUScriptParser.g:147:1: self returns [SelfNode node] : SELF ;
	public final KJUScriptParser.self_return self() throws RecognitionException {
		KJUScriptParser.self_return retval = new KJUScriptParser.self_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token SELF31=null;

		Object SELF31_tree=null;

		try {
			// KJUScriptParser.g:147:29: ( SELF )
			// KJUScriptParser.g:148:5: SELF
			{
			root_0 = (Object)adaptor.nil();


			SELF31=(Token)match(input,SELF,FOLLOW_SELF_in_self1002); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			SELF31_tree = (Object)adaptor.create(SELF31);
			adaptor.addChild(root_0, SELF31_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new SelfNode(); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "self"


	public static class instanceVariable_return extends ParserRuleReturnScope {
		public InstanceVariableNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "instanceVariable"
	// KJUScriptParser.g:152:1: instanceVariable returns [InstanceVariableNode node] : AT NAME ;
	public final KJUScriptParser.instanceVariable_return instanceVariable() throws RecognitionException {
		KJUScriptParser.instanceVariable_return retval = new KJUScriptParser.instanceVariable_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token AT32=null;
		Token NAME33=null;

		Object AT32_tree=null;
		Object NAME33_tree=null;

		try {
			// KJUScriptParser.g:152:53: ( AT NAME )
			// KJUScriptParser.g:153:5: AT NAME
			{
			root_0 = (Object)adaptor.nil();


			AT32=(Token)match(input,AT,FOLLOW_AT_in_instanceVariable1036); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			AT32_tree = (Object)adaptor.create(AT32);
			adaptor.addChild(root_0, AT32_tree);
			}

			NAME33=(Token)match(input,NAME,FOLLOW_NAME_in_instanceVariable1038); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			NAME33_tree = (Object)adaptor.create(NAME33);
			adaptor.addChild(root_0, NAME33_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new InstanceVariableNode((NAME33!=null?NAME33.getText():null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "instanceVariable"


	public static class call_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "call"
	// KJUScriptParser.g:157:1: call returns [Node node] : ( literal DOT )? (head= message DOT )* tail= message ;
	public final KJUScriptParser.call_return call() throws RecognitionException {
		KJUScriptParser.call_return retval = new KJUScriptParser.call_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token DOT35=null;
		Token DOT36=null;
		ParserRuleReturnScope head =null;
		ParserRuleReturnScope tail =null;
		ParserRuleReturnScope literal34 =null;

		Object DOT35_tree=null;
		Object DOT36_tree=null;

		try {
			// KJUScriptParser.g:157:25: ( ( literal DOT )? (head= message DOT )* tail= message )
			// KJUScriptParser.g:158:5: ( literal DOT )? (head= message DOT )* tail= message
			{
			root_0 = (Object)adaptor.nil();


			// KJUScriptParser.g:158:5: ( literal DOT )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==AT||LA15_0==CONSTANT||(LA15_0 >= FALSE && LA15_0 <= FLOAT)||LA15_0==INTEGER||LA15_0==NIL||LA15_0==SELF||LA15_0==STRING||LA15_0==TRUE) ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// KJUScriptParser.g:158:6: literal DOT
					{
					pushFollow(FOLLOW_literal_in_call1070);
					literal34=literal();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, literal34.getTree());

					DOT35=(Token)match(input,DOT,FOLLOW_DOT_in_call1072); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOT35_tree = (Object)adaptor.create(DOT35);
					adaptor.addChild(root_0, DOT35_tree);
					}

					if ( state.backtracking==0 ) { retval.node = (literal34!=null?((KJUScriptParser.literal_return)literal34).node:null); }
					}
					break;

			}

			// KJUScriptParser.g:160:5: (head= message DOT )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==NAME) ) {
					int LA16_1 = input.LA(2);
					if ( (synpred37_KJUScriptParser()) ) {
						alt16=1;
					}

				}

				switch (alt16) {
				case 1 :
					// KJUScriptParser.g:160:6: head= message DOT
					{
					pushFollow(FOLLOW_message_in_call1111);
					head=message();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, head.getTree());

					DOT36=(Token)match(input,DOT,FOLLOW_DOT_in_call1113); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					DOT36_tree = (Object)adaptor.create(DOT36);
					adaptor.addChild(root_0, DOT36_tree);
					}

					if ( state.backtracking==0 ) { ((CallNode)(head!=null?((KJUScriptParser.message_return)head).node:null)).setReceiver(retval.node); retval.node = (head!=null?((KJUScriptParser.message_return)head).node:null); }
					}
					break;

				default :
					break loop16;
				}
			}

			pushFollow(FOLLOW_message_in_call1146);
			tail=message();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, tail.getTree());

			if ( state.backtracking==0 ) { ((CallNode)(tail!=null?((KJUScriptParser.message_return)tail).node:null)).setReceiver(retval.node); retval.node = (tail!=null?((KJUScriptParser.message_return)tail).node:null); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "call"


	public static class message_return extends ParserRuleReturnScope {
		public CallNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "message"
	// KJUScriptParser.g:166:1: message returns [CallNode node] : ( NAME | NAME OPEN_PARENT CLOSE_PARENT | NAME OPEN_PARENT arguments CLOSE_PARENT );
	public final KJUScriptParser.message_return message() throws RecognitionException {
		KJUScriptParser.message_return retval = new KJUScriptParser.message_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NAME37=null;
		Token NAME38=null;
		Token OPEN_PARENT39=null;
		Token CLOSE_PARENT40=null;
		Token NAME41=null;
		Token OPEN_PARENT42=null;
		Token CLOSE_PARENT44=null;
		ParserRuleReturnScope arguments43 =null;

		Object NAME37_tree=null;
		Object NAME38_tree=null;
		Object OPEN_PARENT39_tree=null;
		Object CLOSE_PARENT40_tree=null;
		Object NAME41_tree=null;
		Object OPEN_PARENT42_tree=null;
		Object CLOSE_PARENT44_tree=null;

		try {
			// KJUScriptParser.g:166:32: ( NAME | NAME OPEN_PARENT CLOSE_PARENT | NAME OPEN_PARENT arguments CLOSE_PARENT )
			int alt17=3;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==NAME) ) {
				int LA17_1 = input.LA(2);
				if ( (LA17_1==OPEN_PARENT) ) {
					int LA17_2 = input.LA(3);
					if ( (LA17_2==CLOSE_PARENT) ) {
						alt17=2;
					}
					else if ( (LA17_2==ASPREF||LA17_2==AT||LA17_2==CLASS||(LA17_2 >= CONSTANT && LA17_2 <= DEF)||(LA17_2 >= FALSE && LA17_2 <= FLOAT)||(LA17_2 >= IF && LA17_2 <= INTEGER)||LA17_2==NAME||(LA17_2 >= NIL && LA17_2 <= NOT)||LA17_2==OPEN_PARENT||LA17_2==SELF||LA17_2==STRING||(LA17_2 >= TRUE && LA17_2 <= TRY)||LA17_2==WHILE) ) {
						alt17=3;
					}

					else {
						if (state.backtracking>0) {state.failed=true; return retval;}
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 17, 2, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}
				else if ( (LA17_1==EOF||LA17_1==AND||LA17_1==CATCH||LA17_1==CLOSE_PARENT||LA17_1==COMMA||(LA17_1 >= DIV && LA17_1 <= EQ)||(LA17_1 >= GE && LA17_1 <= GT)||LA17_1==LE||(LA17_1 >= LT && LA17_1 <= MUL)||LA17_1==NEWLINE||(LA17_1 >= OR && LA17_1 <= PLUS)||LA17_1==SEMICOLON) ) {
					alt17=1;
				}

				else {
					if (state.backtracking>0) {state.failed=true; return retval;}
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 17, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 17, 0, input);
				throw nvae;
			}

			switch (alt17) {
				case 1 :
					// KJUScriptParser.g:167:5: NAME
					{
					root_0 = (Object)adaptor.nil();


					NAME37=(Token)match(input,NAME,FOLLOW_NAME_in_message1186); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME37_tree = (Object)adaptor.create(NAME37);
					adaptor.addChild(root_0, NAME37_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new CallNode((NAME37!=null?NAME37.getText():null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:168:5: NAME OPEN_PARENT CLOSE_PARENT
					{
					root_0 = (Object)adaptor.nil();


					NAME38=(Token)match(input,NAME,FOLLOW_NAME_in_message1221); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME38_tree = (Object)adaptor.create(NAME38);
					adaptor.addChild(root_0, NAME38_tree);
					}

					OPEN_PARENT39=(Token)match(input,OPEN_PARENT,FOLLOW_OPEN_PARENT_in_message1223); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					OPEN_PARENT39_tree = (Object)adaptor.create(OPEN_PARENT39);
					adaptor.addChild(root_0, OPEN_PARENT39_tree);
					}

					CLOSE_PARENT40=(Token)match(input,CLOSE_PARENT,FOLLOW_CLOSE_PARENT_in_message1225); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CLOSE_PARENT40_tree = (Object)adaptor.create(CLOSE_PARENT40);
					adaptor.addChild(root_0, CLOSE_PARENT40_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new CallNode((NAME38!=null?NAME38.getText():null), new ArrayList<Node>()); }
					}
					break;
				case 3 :
					// KJUScriptParser.g:169:5: NAME OPEN_PARENT arguments CLOSE_PARENT
					{
					root_0 = (Object)adaptor.nil();


					NAME41=(Token)match(input,NAME,FOLLOW_NAME_in_message1235); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME41_tree = (Object)adaptor.create(NAME41);
					adaptor.addChild(root_0, NAME41_tree);
					}

					OPEN_PARENT42=(Token)match(input,OPEN_PARENT,FOLLOW_OPEN_PARENT_in_message1237); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					OPEN_PARENT42_tree = (Object)adaptor.create(OPEN_PARENT42);
					adaptor.addChild(root_0, OPEN_PARENT42_tree);
					}

					pushFollow(FOLLOW_arguments_in_message1250);
					arguments43=arguments();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, arguments43.getTree());

					CLOSE_PARENT44=(Token)match(input,CLOSE_PARENT,FOLLOW_CLOSE_PARENT_in_message1261); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CLOSE_PARENT44_tree = (Object)adaptor.create(CLOSE_PARENT44);
					adaptor.addChild(root_0, CLOSE_PARENT44_tree);
					}

					if ( state.backtracking==0 ) { retval.node = new CallNode((NAME41!=null?NAME41.getText():null), (arguments43!=null?((KJUScriptParser.arguments_return)arguments43).nodes:null)); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "message"


	public static class arguments_return extends ParserRuleReturnScope {
		public ArrayList<Node> nodes;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arguments"
	// KJUScriptParser.g:175:1: arguments returns [ArrayList<Node> nodes] :head= expression ( COMMA tail= expression )* ;
	public final KJUScriptParser.arguments_return arguments() throws RecognitionException {
		KJUScriptParser.arguments_return retval = new KJUScriptParser.arguments_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token COMMA45=null;
		ParserRuleReturnScope head =null;
		ParserRuleReturnScope tail =null;

		Object COMMA45_tree=null;

		try {
			// KJUScriptParser.g:175:42: (head= expression ( COMMA tail= expression )* )
			// KJUScriptParser.g:176:37: head= expression ( COMMA tail= expression )*
			{
			root_0 = (Object)adaptor.nil();


			if ( state.backtracking==0 ) { retval.nodes = new ArrayList<Node>(); }
			pushFollow(FOLLOW_expression_in_arguments1336);
			head=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, head.getTree());

			if ( state.backtracking==0 ) { retval.nodes.add((head!=null?((KJUScriptParser.expression_return)head).node:null)); }
			// KJUScriptParser.g:178:5: ( COMMA tail= expression )*
			loop18:
			while (true) {
				int alt18=2;
				int LA18_0 = input.LA(1);
				if ( (LA18_0==COMMA) ) {
					alt18=1;
				}

				switch (alt18) {
				case 1 :
					// KJUScriptParser.g:178:6: COMMA tail= expression
					{
					COMMA45=(Token)match(input,COMMA,FOLLOW_COMMA_in_arguments1361); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					COMMA45_tree = (Object)adaptor.create(COMMA45);
					adaptor.addChild(root_0, COMMA45_tree);
					}

					pushFollow(FOLLOW_expression_in_arguments1370);
					tail=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, tail.getTree());

					if ( state.backtracking==0 ) { retval.nodes.add((tail!=null?((KJUScriptParser.expression_return)tail).node:null)); }
					}
					break;

				default :
					break loop18;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arguments"


	public static class constant_return extends ParserRuleReturnScope {
		public ConstantNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "constant"
	// KJUScriptParser.g:184:1: constant returns [ConstantNode node] : CONSTANT ;
	public final KJUScriptParser.constant_return constant() throws RecognitionException {
		KJUScriptParser.constant_return retval = new KJUScriptParser.constant_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token CONSTANT46=null;

		Object CONSTANT46_tree=null;

		try {
			// KJUScriptParser.g:184:37: ( CONSTANT )
			// KJUScriptParser.g:185:5: CONSTANT
			{
			root_0 = (Object)adaptor.nil();


			CONSTANT46=(Token)match(input,CONSTANT,FOLLOW_CONSTANT_in_constant1413); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			CONSTANT46_tree = (Object)adaptor.create(CONSTANT46);
			adaptor.addChild(root_0, CONSTANT46_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new ConstantNode((CONSTANT46!=null?CONSTANT46.getText():null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "constant"


	public static class assign_return extends ParserRuleReturnScope {
		public Node node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "assign"
	// KJUScriptParser.g:189:1: assign returns [Node node] : ( NAME ASSIGN expression | ASPREF NAME ASSIGN expression | CONSTANT ASSIGN expression | AT NAME ASSIGN expression );
	public final KJUScriptParser.assign_return assign() throws RecognitionException {
		KJUScriptParser.assign_return retval = new KJUScriptParser.assign_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NAME47=null;
		Token ASSIGN48=null;
		Token ASPREF50=null;
		Token NAME51=null;
		Token ASSIGN52=null;
		Token CONSTANT54=null;
		Token ASSIGN55=null;
		Token AT57=null;
		Token NAME58=null;
		Token ASSIGN59=null;
		ParserRuleReturnScope expression49 =null;
		ParserRuleReturnScope expression53 =null;
		ParserRuleReturnScope expression56 =null;
		ParserRuleReturnScope expression60 =null;

		Object NAME47_tree=null;
		Object ASSIGN48_tree=null;
		Object ASPREF50_tree=null;
		Object NAME51_tree=null;
		Object ASSIGN52_tree=null;
		Object CONSTANT54_tree=null;
		Object ASSIGN55_tree=null;
		Object AT57_tree=null;
		Object NAME58_tree=null;
		Object ASSIGN59_tree=null;

		try {
			// KJUScriptParser.g:189:27: ( NAME ASSIGN expression | ASPREF NAME ASSIGN expression | CONSTANT ASSIGN expression | AT NAME ASSIGN expression )
			int alt19=4;
			switch ( input.LA(1) ) {
			case NAME:
				{
				alt19=1;
				}
				break;
			case ASPREF:
				{
				alt19=2;
				}
				break;
			case CONSTANT:
				{
				alt19=3;
				}
				break;
			case AT:
				{
				alt19=4;
				}
				break;
			default:
				if (state.backtracking>0) {state.failed=true; return retval;}
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}
			switch (alt19) {
				case 1 :
					// KJUScriptParser.g:190:5: NAME ASSIGN expression
					{
					root_0 = (Object)adaptor.nil();


					NAME47=(Token)match(input,NAME,FOLLOW_NAME_in_assign1457); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME47_tree = (Object)adaptor.create(NAME47);
					adaptor.addChild(root_0, NAME47_tree);
					}

					ASSIGN48=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_assign1459); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ASSIGN48_tree = (Object)adaptor.create(ASSIGN48);
					adaptor.addChild(root_0, ASSIGN48_tree);
					}

					pushFollow(FOLLOW_expression_in_assign1461);
					expression49=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression49.getTree());

					if ( state.backtracking==0 ) { retval.node = new LocalAssignNode((NAME47!=null?NAME47.getText():null), (expression49!=null?((KJUScriptParser.expression_return)expression49).node:null)); }
					}
					break;
				case 2 :
					// KJUScriptParser.g:191:5: ASPREF NAME ASSIGN expression
					{
					root_0 = (Object)adaptor.nil();


					ASPREF50=(Token)match(input,ASPREF,FOLLOW_ASPREF_in_assign1478); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ASPREF50_tree = (Object)adaptor.create(ASPREF50);
					adaptor.addChild(root_0, ASPREF50_tree);
					}

					NAME51=(Token)match(input,NAME,FOLLOW_NAME_in_assign1480); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME51_tree = (Object)adaptor.create(NAME51);
					adaptor.addChild(root_0, NAME51_tree);
					}

					ASSIGN52=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_assign1482); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ASSIGN52_tree = (Object)adaptor.create(ASSIGN52);
					adaptor.addChild(root_0, ASSIGN52_tree);
					}

					pushFollow(FOLLOW_expression_in_assign1484);
					expression53=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression53.getTree());

					if ( state.backtracking==0 ) { retval.node = new LocalAssignNode((NAME51!=null?NAME51.getText():null), (expression53!=null?((KJUScriptParser.expression_return)expression53).node:null)); }
					}
					break;
				case 3 :
					// KJUScriptParser.g:192:5: CONSTANT ASSIGN expression
					{
					root_0 = (Object)adaptor.nil();


					CONSTANT54=(Token)match(input,CONSTANT,FOLLOW_CONSTANT_in_assign1494); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CONSTANT54_tree = (Object)adaptor.create(CONSTANT54);
					adaptor.addChild(root_0, CONSTANT54_tree);
					}

					ASSIGN55=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_assign1496); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ASSIGN55_tree = (Object)adaptor.create(ASSIGN55);
					adaptor.addChild(root_0, ASSIGN55_tree);
					}

					pushFollow(FOLLOW_expression_in_assign1498);
					expression56=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression56.getTree());

					if ( state.backtracking==0 ) { retval.node = new ConstantAssignNode((CONSTANT54!=null?CONSTANT54.getText():null), (expression56!=null?((KJUScriptParser.expression_return)expression56).node:null)); }
					}
					break;
				case 4 :
					// KJUScriptParser.g:193:5: AT NAME ASSIGN expression
					{
					root_0 = (Object)adaptor.nil();


					AT57=(Token)match(input,AT,FOLLOW_AT_in_assign1511); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					AT57_tree = (Object)adaptor.create(AT57);
					adaptor.addChild(root_0, AT57_tree);
					}

					NAME58=(Token)match(input,NAME,FOLLOW_NAME_in_assign1513); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME58_tree = (Object)adaptor.create(NAME58);
					adaptor.addChild(root_0, NAME58_tree);
					}

					ASSIGN59=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_assign1515); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ASSIGN59_tree = (Object)adaptor.create(ASSIGN59);
					adaptor.addChild(root_0, ASSIGN59_tree);
					}

					pushFollow(FOLLOW_expression_in_assign1517);
					expression60=expression();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, expression60.getTree());

					if ( state.backtracking==0 ) { retval.node = new InstanceVariableAssignNode((NAME58!=null?NAME58.getText():null), (expression60!=null?((KJUScriptParser.expression_return)expression60).node:null)); }
					}
					break;

			}
			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assign"


	public static class methodDefinition_return extends ParserRuleReturnScope {
		public MethodDefinitionNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodDefinition"
	// KJUScriptParser.g:196:1: methodDefinition returns [MethodDefinitionNode node] : DEF NAME ( OPEN_PARENT ( parameters )? CLOSE_PARENT ) ( TOBE )? terminator expressions END ;
	public final KJUScriptParser.methodDefinition_return methodDefinition() throws RecognitionException {
		KJUScriptParser.methodDefinition_return retval = new KJUScriptParser.methodDefinition_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token DEF61=null;
		Token NAME62=null;
		Token OPEN_PARENT63=null;
		Token CLOSE_PARENT65=null;
		Token TOBE66=null;
		Token END69=null;
		ParserRuleReturnScope parameters64 =null;
		ParserRuleReturnScope terminator67 =null;
		ParserRuleReturnScope expressions68 =null;

		Object DEF61_tree=null;
		Object NAME62_tree=null;
		Object OPEN_PARENT63_tree=null;
		Object CLOSE_PARENT65_tree=null;
		Object TOBE66_tree=null;
		Object END69_tree=null;

		try {
			// KJUScriptParser.g:196:53: ( DEF NAME ( OPEN_PARENT ( parameters )? CLOSE_PARENT ) ( TOBE )? terminator expressions END )
			// KJUScriptParser.g:197:5: DEF NAME ( OPEN_PARENT ( parameters )? CLOSE_PARENT ) ( TOBE )? terminator expressions END
			{
			root_0 = (Object)adaptor.nil();


			DEF61=(Token)match(input,DEF,FOLLOW_DEF_in_methodDefinition1543); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			DEF61_tree = (Object)adaptor.create(DEF61);
			adaptor.addChild(root_0, DEF61_tree);
			}

			NAME62=(Token)match(input,NAME,FOLLOW_NAME_in_methodDefinition1545); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			NAME62_tree = (Object)adaptor.create(NAME62);
			adaptor.addChild(root_0, NAME62_tree);
			}

			// KJUScriptParser.g:197:14: ( OPEN_PARENT ( parameters )? CLOSE_PARENT )
			// KJUScriptParser.g:197:15: OPEN_PARENT ( parameters )? CLOSE_PARENT
			{
			OPEN_PARENT63=(Token)match(input,OPEN_PARENT,FOLLOW_OPEN_PARENT_in_methodDefinition1548); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			OPEN_PARENT63_tree = (Object)adaptor.create(OPEN_PARENT63);
			adaptor.addChild(root_0, OPEN_PARENT63_tree);
			}

			// KJUScriptParser.g:197:27: ( parameters )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==NAME) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// KJUScriptParser.g:197:27: parameters
					{
					pushFollow(FOLLOW_parameters_in_methodDefinition1550);
					parameters64=parameters();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, parameters64.getTree());

					}
					break;

			}

			CLOSE_PARENT65=(Token)match(input,CLOSE_PARENT,FOLLOW_CLOSE_PARENT_in_methodDefinition1553); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			CLOSE_PARENT65_tree = (Object)adaptor.create(CLOSE_PARENT65);
			adaptor.addChild(root_0, CLOSE_PARENT65_tree);
			}

			}

			// KJUScriptParser.g:197:53: ( TOBE )?
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==TOBE) ) {
				alt21=1;
			}
			switch (alt21) {
				case 1 :
					// KJUScriptParser.g:197:53: TOBE
					{
					TOBE66=(Token)match(input,TOBE,FOLLOW_TOBE_in_methodDefinition1556); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					TOBE66_tree = (Object)adaptor.create(TOBE66);
					adaptor.addChild(root_0, TOBE66_tree);
					}

					}
					break;

			}

			pushFollow(FOLLOW_terminator_in_methodDefinition1560);
			terminator67=terminator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator67.getTree());

			pushFollow(FOLLOW_expressions_in_methodDefinition1568);
			expressions68=expressions();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, expressions68.getTree());

			END69=(Token)match(input,END,FOLLOW_END_in_methodDefinition1574); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			END69_tree = (Object)adaptor.create(END69);
			adaptor.addChild(root_0, END69_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new MethodDefinitionNode((NAME62!=null?NAME62.getText():null), (parameters64!=null?((KJUScriptParser.parameters_return)parameters64).names:null), (expressions68!=null?((KJUScriptParser.expressions_return)expressions68).nodes:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodDefinition"


	public static class parameters_return extends ParserRuleReturnScope {
		public ArrayList<String> names;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "parameters"
	// KJUScriptParser.g:203:1: parameters returns [ArrayList<String> names] :head= NAME ( COMMA tail= NAME )* ;
	public final KJUScriptParser.parameters_return parameters() throws RecognitionException {
		KJUScriptParser.parameters_return retval = new KJUScriptParser.parameters_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token head=null;
		Token tail=null;
		Token COMMA70=null;

		Object head_tree=null;
		Object tail_tree=null;
		Object COMMA70_tree=null;

		try {
			// KJUScriptParser.g:203:45: (head= NAME ( COMMA tail= NAME )* )
			// KJUScriptParser.g:204:37: head= NAME ( COMMA tail= NAME )*
			{
			root_0 = (Object)adaptor.nil();


			if ( state.backtracking==0 ) { retval.names = new ArrayList<String>(); }
			head=(Token)match(input,NAME,FOLLOW_NAME_in_parameters1663); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			head_tree = (Object)adaptor.create(head);
			adaptor.addChild(root_0, head_tree);
			}

			if ( state.backtracking==0 ) { retval.names.add((head!=null?head.getText():null)); }
			// KJUScriptParser.g:206:5: ( COMMA tail= NAME )*
			loop22:
			while (true) {
				int alt22=2;
				int LA22_0 = input.LA(1);
				if ( (LA22_0==COMMA) ) {
					alt22=1;
				}

				switch (alt22) {
				case 1 :
					// KJUScriptParser.g:206:6: COMMA tail= NAME
					{
					COMMA70=(Token)match(input,COMMA,FOLLOW_COMMA_in_parameters1694); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					COMMA70_tree = (Object)adaptor.create(COMMA70);
					adaptor.addChild(root_0, COMMA70_tree);
					}

					tail=(Token)match(input,NAME,FOLLOW_NAME_in_parameters1703); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					tail_tree = (Object)adaptor.create(tail);
					adaptor.addChild(root_0, tail_tree);
					}

					if ( state.backtracking==0 ) { retval.names.add((tail!=null?tail.getText():null)); }
					}
					break;

				default :
					break loop22;
				}
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "parameters"


	public static class classDefinition_return extends ParserRuleReturnScope {
		public ClassDefinitionNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "classDefinition"
	// KJUScriptParser.g:211:1: classDefinition returns [ClassDefinitionNode node] : CLASS name= CONSTANT ( LT superClass= CONSTANT )? terminator expressions END ;
	public final KJUScriptParser.classDefinition_return classDefinition() throws RecognitionException {
		KJUScriptParser.classDefinition_return retval = new KJUScriptParser.classDefinition_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token name=null;
		Token superClass=null;
		Token CLASS71=null;
		Token LT72=null;
		Token END75=null;
		ParserRuleReturnScope terminator73 =null;
		ParserRuleReturnScope expressions74 =null;

		Object name_tree=null;
		Object superClass_tree=null;
		Object CLASS71_tree=null;
		Object LT72_tree=null;
		Object END75_tree=null;

		try {
			// KJUScriptParser.g:211:51: ( CLASS name= CONSTANT ( LT superClass= CONSTANT )? terminator expressions END )
			// KJUScriptParser.g:212:5: CLASS name= CONSTANT ( LT superClass= CONSTANT )? terminator expressions END
			{
			root_0 = (Object)adaptor.nil();


			CLASS71=(Token)match(input,CLASS,FOLLOW_CLASS_in_classDefinition1751); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			CLASS71_tree = (Object)adaptor.create(CLASS71);
			adaptor.addChild(root_0, CLASS71_tree);
			}

			name=(Token)match(input,CONSTANT,FOLLOW_CONSTANT_in_classDefinition1755); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			name_tree = (Object)adaptor.create(name);
			adaptor.addChild(root_0, name_tree);
			}

			// KJUScriptParser.g:212:25: ( LT superClass= CONSTANT )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==LT) ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// KJUScriptParser.g:212:26: LT superClass= CONSTANT
					{
					LT72=(Token)match(input,LT,FOLLOW_LT_in_classDefinition1758); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					LT72_tree = (Object)adaptor.create(LT72);
					adaptor.addChild(root_0, LT72_tree);
					}

					superClass=(Token)match(input,CONSTANT,FOLLOW_CONSTANT_in_classDefinition1762); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					superClass_tree = (Object)adaptor.create(superClass);
					adaptor.addChild(root_0, superClass_tree);
					}

					}
					break;

			}

			pushFollow(FOLLOW_terminator_in_classDefinition1766);
			terminator73=terminator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator73.getTree());

			pushFollow(FOLLOW_expressions_in_classDefinition1774);
			expressions74=expressions();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, expressions74.getTree());

			END75=(Token)match(input,END,FOLLOW_END_in_classDefinition1780); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			END75_tree = (Object)adaptor.create(END75);
			adaptor.addChild(root_0, END75_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new ClassDefinitionNode((name!=null?name.getText():null), (superClass!=null?superClass.getText():null), (expressions74!=null?((KJUScriptParser.expressions_return)expressions74).nodes:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "classDefinition"


	public static class ifBlock_return extends ParserRuleReturnScope {
		public IfNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "ifBlock"
	// KJUScriptParser.g:217:1: ifBlock returns [IfNode node] : IF condition= expression terminator ifBody= expressions ( ELSE terminator elseBody= expressions )? END ;
	public final KJUScriptParser.ifBlock_return ifBlock() throws RecognitionException {
		KJUScriptParser.ifBlock_return retval = new KJUScriptParser.ifBlock_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token IF76=null;
		Token ELSE78=null;
		Token END80=null;
		ParserRuleReturnScope condition =null;
		ParserRuleReturnScope ifBody =null;
		ParserRuleReturnScope elseBody =null;
		ParserRuleReturnScope terminator77 =null;
		ParserRuleReturnScope terminator79 =null;

		Object IF76_tree=null;
		Object ELSE78_tree=null;
		Object END80_tree=null;

		try {
			// KJUScriptParser.g:217:30: ( IF condition= expression terminator ifBody= expressions ( ELSE terminator elseBody= expressions )? END )
			// KJUScriptParser.g:218:5: IF condition= expression terminator ifBody= expressions ( ELSE terminator elseBody= expressions )? END
			{
			root_0 = (Object)adaptor.nil();


			IF76=(Token)match(input,IF,FOLLOW_IF_in_ifBlock1828); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			IF76_tree = (Object)adaptor.create(IF76);
			adaptor.addChild(root_0, IF76_tree);
			}

			pushFollow(FOLLOW_expression_in_ifBlock1832);
			condition=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, condition.getTree());

			pushFollow(FOLLOW_terminator_in_ifBlock1834);
			terminator77=terminator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator77.getTree());

			pushFollow(FOLLOW_expressions_in_ifBlock1844);
			ifBody=expressions();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, ifBody.getTree());

			// KJUScriptParser.g:220:5: ( ELSE terminator elseBody= expressions )?
			int alt24=2;
			int LA24_0 = input.LA(1);
			if ( (LA24_0==ELSE) ) {
				alt24=1;
			}
			switch (alt24) {
				case 1 :
					// KJUScriptParser.g:220:6: ELSE terminator elseBody= expressions
					{
					ELSE78=(Token)match(input,ELSE,FOLLOW_ELSE_in_ifBlock1851); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					ELSE78_tree = (Object)adaptor.create(ELSE78);
					adaptor.addChild(root_0, ELSE78_tree);
					}

					pushFollow(FOLLOW_terminator_in_ifBlock1853);
					terminator79=terminator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator79.getTree());

					pushFollow(FOLLOW_expressions_in_ifBlock1863);
					elseBody=expressions();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, elseBody.getTree());

					}
					break;

			}

			END80=(Token)match(input,END,FOLLOW_END_in_ifBlock1876); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			END80_tree = (Object)adaptor.create(END80);
			adaptor.addChild(root_0, END80_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new IfNode((condition!=null?((KJUScriptParser.expression_return)condition).node:null), (ifBody!=null?((KJUScriptParser.expressions_return)ifBody).nodes:null), (elseBody!=null?((KJUScriptParser.expressions_return)elseBody).nodes:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ifBlock"


	public static class whileBlock_return extends ParserRuleReturnScope {
		public WhileNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "whileBlock"
	// KJUScriptParser.g:226:1: whileBlock returns [WhileNode node] : WHILE condition= expression terminator body= expressions END ;
	public final KJUScriptParser.whileBlock_return whileBlock() throws RecognitionException {
		KJUScriptParser.whileBlock_return retval = new KJUScriptParser.whileBlock_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token WHILE81=null;
		Token END83=null;
		ParserRuleReturnScope condition =null;
		ParserRuleReturnScope body =null;
		ParserRuleReturnScope terminator82 =null;

		Object WHILE81_tree=null;
		Object END83_tree=null;

		try {
			// KJUScriptParser.g:226:36: ( WHILE condition= expression terminator body= expressions END )
			// KJUScriptParser.g:227:5: WHILE condition= expression terminator body= expressions END
			{
			root_0 = (Object)adaptor.nil();


			WHILE81=(Token)match(input,WHILE,FOLLOW_WHILE_in_whileBlock1924); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			WHILE81_tree = (Object)adaptor.create(WHILE81);
			adaptor.addChild(root_0, WHILE81_tree);
			}

			pushFollow(FOLLOW_expression_in_whileBlock1928);
			condition=expression();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, condition.getTree());

			pushFollow(FOLLOW_terminator_in_whileBlock1930);
			terminator82=terminator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator82.getTree());

			pushFollow(FOLLOW_expressions_in_whileBlock1940);
			body=expressions();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, body.getTree());

			END83=(Token)match(input,END,FOLLOW_END_in_whileBlock1946); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			END83_tree = (Object)adaptor.create(END83);
			adaptor.addChild(root_0, END83_tree);
			}

			if ( state.backtracking==0 ) { retval.node = new WhileNode((condition!=null?((KJUScriptParser.expression_return)condition).node:null), (body!=null?((KJUScriptParser.expressions_return)body).nodes:null)); }
			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "whileBlock"


	public static class tryBlock_return extends ParserRuleReturnScope {
		public TryNode node;
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "tryBlock"
	// KJUScriptParser.g:232:1: tryBlock returns [TryNode node] : TRY terminator tryBody= expressions ( CATCH CONSTANT COLON NAME terminator catchBody= expressions )* END ;
	public final KJUScriptParser.tryBlock_return tryBlock() throws RecognitionException {
		KJUScriptParser.tryBlock_return retval = new KJUScriptParser.tryBlock_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token TRY84=null;
		Token CATCH86=null;
		Token CONSTANT87=null;
		Token COLON88=null;
		Token NAME89=null;
		Token END91=null;
		ParserRuleReturnScope tryBody =null;
		ParserRuleReturnScope catchBody =null;
		ParserRuleReturnScope terminator85 =null;
		ParserRuleReturnScope terminator90 =null;

		Object TRY84_tree=null;
		Object CATCH86_tree=null;
		Object CONSTANT87_tree=null;
		Object COLON88_tree=null;
		Object NAME89_tree=null;
		Object END91_tree=null;

		try {
			// KJUScriptParser.g:232:32: ( TRY terminator tryBody= expressions ( CATCH CONSTANT COLON NAME terminator catchBody= expressions )* END )
			// KJUScriptParser.g:233:5: TRY terminator tryBody= expressions ( CATCH CONSTANT COLON NAME terminator catchBody= expressions )* END
			{
			root_0 = (Object)adaptor.nil();


			TRY84=(Token)match(input,TRY,FOLLOW_TRY_in_tryBlock1994); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			TRY84_tree = (Object)adaptor.create(TRY84);
			adaptor.addChild(root_0, TRY84_tree);
			}

			pushFollow(FOLLOW_terminator_in_tryBlock1996);
			terminator85=terminator();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator85.getTree());

			pushFollow(FOLLOW_expressions_in_tryBlock2006);
			tryBody=expressions();
			state._fsp--;
			if (state.failed) return retval;
			if ( state.backtracking==0 ) adaptor.addChild(root_0, tryBody.getTree());

			if ( state.backtracking==0 ) { retval.node = new TryNode((tryBody!=null?((KJUScriptParser.expressions_return)tryBody).nodes:null)); }
			// KJUScriptParser.g:235:5: ( CATCH CONSTANT COLON NAME terminator catchBody= expressions )*
			loop25:
			while (true) {
				int alt25=2;
				int LA25_0 = input.LA(1);
				if ( (LA25_0==CATCH) ) {
					alt25=1;
				}

				switch (alt25) {
				case 1 :
					// KJUScriptParser.g:235:6: CATCH CONSTANT COLON NAME terminator catchBody= expressions
					{
					CATCH86=(Token)match(input,CATCH,FOLLOW_CATCH_in_tryBlock2033); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CATCH86_tree = (Object)adaptor.create(CATCH86);
					adaptor.addChild(root_0, CATCH86_tree);
					}

					CONSTANT87=(Token)match(input,CONSTANT,FOLLOW_CONSTANT_in_tryBlock2035); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					CONSTANT87_tree = (Object)adaptor.create(CONSTANT87);
					adaptor.addChild(root_0, CONSTANT87_tree);
					}

					COLON88=(Token)match(input,COLON,FOLLOW_COLON_in_tryBlock2037); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					COLON88_tree = (Object)adaptor.create(COLON88);
					adaptor.addChild(root_0, COLON88_tree);
					}

					NAME89=(Token)match(input,NAME,FOLLOW_NAME_in_tryBlock2039); if (state.failed) return retval;
					if ( state.backtracking==0 ) {
					NAME89_tree = (Object)adaptor.create(NAME89);
					adaptor.addChild(root_0, NAME89_tree);
					}

					pushFollow(FOLLOW_terminator_in_tryBlock2041);
					terminator90=terminator();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, terminator90.getTree());

					pushFollow(FOLLOW_expressions_in_tryBlock2051);
					catchBody=expressions();
					state._fsp--;
					if (state.failed) return retval;
					if ( state.backtracking==0 ) adaptor.addChild(root_0, catchBody.getTree());

					if ( state.backtracking==0 ) { retval.node.addCatchBlock((CONSTANT87!=null?CONSTANT87.getText():null), (NAME89!=null?NAME89.getText():null), (catchBody!=null?((KJUScriptParser.expressions_return)catchBody).nodes:null));  }
					}
					break;

				default :
					break loop25;
				}
			}

			END91=(Token)match(input,END,FOLLOW_END_in_tryBlock2082); if (state.failed) return retval;
			if ( state.backtracking==0 ) {
			END91_tree = (Object)adaptor.create(END91);
			adaptor.addChild(root_0, END91_tree);
			}

			}

			retval.stop = input.LT(-1);

			if ( state.backtracking==0 ) {
			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
			}
		}

		  catch(RecognitionException recognitionException) {
		    throw recognitionException;
		  }

		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "tryBlock"

	// $ANTLR start synpred8_KJUScriptParser
	public final void synpred8_KJUScriptParser_fragment() throws RecognitionException {
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;


		// KJUScriptParser.g:84:5: (receiver= andExpression OR arg= orExpression )
		// KJUScriptParser.g:84:5: receiver= andExpression OR arg= orExpression
		{
		pushFollow(FOLLOW_andExpression_in_synpred8_KJUScriptParser285);
		receiver=andExpression();
		state._fsp--;
		if (state.failed) return;

		match(input,OR,FOLLOW_OR_in_synpred8_KJUScriptParser293); if (state.failed) return;

		pushFollow(FOLLOW_orExpression_in_synpred8_KJUScriptParser297);
		arg=orExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred8_KJUScriptParser

	// $ANTLR start synpred9_KJUScriptParser
	public final void synpred9_KJUScriptParser_fragment() throws RecognitionException {
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;


		// KJUScriptParser.g:90:5: (receiver= relationalExpression AND arg= andExpression )
		// KJUScriptParser.g:90:5: receiver= relationalExpression AND arg= andExpression
		{
		pushFollow(FOLLOW_relationalExpression_in_synpred9_KJUScriptParser347);
		receiver=relationalExpression();
		state._fsp--;
		if (state.failed) return;

		match(input,AND,FOLLOW_AND_in_synpred9_KJUScriptParser355); if (state.failed) return;

		pushFollow(FOLLOW_andExpression_in_synpred9_KJUScriptParser359);
		arg=andExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred9_KJUScriptParser

	// $ANTLR start synpred14_KJUScriptParser
	public final void synpred14_KJUScriptParser_fragment() throws RecognitionException {
		Token op=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;


		// KJUScriptParser.g:96:5: (receiver= additiveExpression op= ( EQ | LE | GE | LT | GT ) arg= relationalExpression )
		// KJUScriptParser.g:96:5: receiver= additiveExpression op= ( EQ | LE | GE | LT | GT ) arg= relationalExpression
		{
		pushFollow(FOLLOW_additiveExpression_in_synpred14_KJUScriptParser400);
		receiver=additiveExpression();
		state._fsp--;
		if (state.failed) return;

		op=input.LT(1);
		if ( input.LA(1)==EQ||(input.LA(1) >= GE && input.LA(1) <= GT)||input.LA(1)==LE||input.LA(1)==LT ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			throw mse;
		}
		pushFollow(FOLLOW_relationalExpression_in_synpred14_KJUScriptParser430);
		arg=relationalExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred14_KJUScriptParser

	// $ANTLR start synpred16_KJUScriptParser
	public final void synpred16_KJUScriptParser_fragment() throws RecognitionException {
		Token op=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;


		// KJUScriptParser.g:103:5: (receiver= multiplicativeExpression op= ( PLUS | MINUS ) arg= additiveExpression )
		// KJUScriptParser.g:103:5: receiver= multiplicativeExpression op= ( PLUS | MINUS ) arg= additiveExpression
		{
		pushFollow(FOLLOW_multiplicativeExpression_in_synpred16_KJUScriptParser470);
		receiver=multiplicativeExpression();
		state._fsp--;
		if (state.failed) return;

		op=input.LT(1);
		if ( input.LA(1)==MINUS||input.LA(1)==PLUS ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			throw mse;
		}
		pushFollow(FOLLOW_additiveExpression_in_synpred16_KJUScriptParser488);
		arg=additiveExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred16_KJUScriptParser

	// $ANTLR start synpred19_KJUScriptParser
	public final void synpred19_KJUScriptParser_fragment() throws RecognitionException {
		Token op=null;
		ParserRuleReturnScope receiver =null;
		ParserRuleReturnScope arg =null;


		// KJUScriptParser.g:109:5: (receiver= unaryExpression op= ( MUL | DIV | MOD ) arg= multiplicativeExpression )
		// KJUScriptParser.g:109:5: receiver= unaryExpression op= ( MUL | DIV | MOD ) arg= multiplicativeExpression
		{
		pushFollow(FOLLOW_unaryExpression_in_synpred19_KJUScriptParser536);
		receiver=unaryExpression();
		state._fsp--;
		if (state.failed) return;

		op=input.LT(1);
		if ( input.LA(1)==DIV||(input.LA(1) >= MOD && input.LA(1) <= MUL) ) {
			input.consume();
			state.errorRecovery=false;
			state.failed=false;
		}
		else {
			if (state.backtracking>0) {state.failed=true; return;}
			MismatchedSetException mse = new MismatchedSetException(null,input);
			throw mse;
		}
		pushFollow(FOLLOW_multiplicativeExpression_in_synpred19_KJUScriptParser556);
		arg=multiplicativeExpression();
		state._fsp--;
		if (state.failed) return;

		}

	}
	// $ANTLR end synpred19_KJUScriptParser

	// $ANTLR start synpred37_KJUScriptParser
	public final void synpred37_KJUScriptParser_fragment() throws RecognitionException {
		ParserRuleReturnScope head =null;


		// KJUScriptParser.g:160:6: (head= message DOT )
		// KJUScriptParser.g:160:6: head= message DOT
		{
		pushFollow(FOLLOW_message_in_synpred37_KJUScriptParser1111);
		head=message();
		state._fsp--;
		if (state.failed) return;

		match(input,DOT,FOLLOW_DOT_in_synpred37_KJUScriptParser1113); if (state.failed) return;

		}

	}
	// $ANTLR end synpred37_KJUScriptParser

	// Delegated rules

	public final boolean synpred19_KJUScriptParser() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred19_KJUScriptParser_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred16_KJUScriptParser() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred16_KJUScriptParser_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred37_KJUScriptParser() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred37_KJUScriptParser_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred8_KJUScriptParser() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred8_KJUScriptParser_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred14_KJUScriptParser() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred14_KJUScriptParser_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}
	public final boolean synpred9_KJUScriptParser() {
		state.backtracking++;
		int start = input.mark();
		try {
			synpred9_KJUScriptParser_fragment(); // can never throw exception
		} catch (RecognitionException re) {
			System.err.println("impossible: "+re);
		}
		boolean success = !state.failed;
		input.rewind(start);
		state.backtracking--;
		state.failed=false;
		return success;
	}


	protected DFA3 dfa3 = new DFA3(this);
	static final String DFA3_eotS =
		"\4\uffff";
	static final String DFA3_eofS =
		"\2\2\2\uffff";
	static final String DFA3_minS =
		"\1\10\1\5\2\uffff";
	static final String DFA3_maxS =
		"\1\56\1\65\2\uffff";
	static final String DFA3_acceptS =
		"\2\uffff\1\2\1\1";
	static final String DFA3_specialS =
		"\4\uffff}>";
	static final String[] DFA3_transitionS = {
			"\1\2\12\uffff\2\2\21\uffff\1\1\7\uffff\1\1",
			"\1\3\1\uffff\1\3\1\2\1\3\4\uffff\2\3\3\uffff\2\2\1\uffff\2\3\3\uffff"+
			"\2\3\10\uffff\1\3\1\1\2\3\1\uffff\1\3\2\uffff\1\3\1\1\1\uffff\1\3\1\uffff"+
			"\2\3\1\uffff\1\3",
			"",
			""
	};

	static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
	static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
	static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
	static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
	static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
	static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
	static final short[][] DFA3_transition;

	static {
		int numStates = DFA3_transitionS.length;
		DFA3_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
		}
	}

	protected class DFA3 extends DFA {

		public DFA3(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 3;
			this.eot = DFA3_eot;
			this.eof = DFA3_eof;
			this.min = DFA3_min;
			this.max = DFA3_max;
			this.accept = DFA3_accept;
			this.special = DFA3_special;
			this.transition = DFA3_transition;
		}
		@Override
		public String getDescription() {
			return "()* loopback of 60:5: ( terminator tail= expression )*";
		}
	}

	public static final BitSet FOLLOW_terminator_in_root77 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_root80 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_root83 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_expressions131 = new BitSet(new long[]{0x0000404000000002L});
	public static final BitSet FOLLOW_terminator_in_expressions142 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_expressions151 = new BitSet(new long[]{0x0000404000000002L});
	public static final BitSet FOLLOW_terminator_in_expressions167 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignExpression_in_expression187 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assign_in_assignExpression231 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_orExpression_in_assignExpression256 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_andExpression_in_orExpression285 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_OR_in_orExpression293 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_orExpression_in_orExpression297 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_andExpression_in_orExpression313 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_relationalExpression_in_andExpression347 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_AND_in_andExpression355 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_andExpression_in_andExpression359 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_relationalExpression_in_andExpression373 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_additiveExpression_in_relationalExpression400 = new BitSet(new long[]{0x0000000243200000L});
	public static final BitSet FOLLOW_set_in_relationalExpression410 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_relationalExpression_in_relationalExpression430 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_additiveExpression_in_relationalExpression441 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression470 = new BitSet(new long[]{0x0000100400000000L});
	public static final BitSet FOLLOW_set_in_additiveExpression480 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_additiveExpression_in_additiveExpression488 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression499 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression536 = new BitSet(new long[]{0x0000001800020000L});
	public static final BitSet FOLLOW_set_in_multiplicativeExpression546 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_multiplicativeExpression556 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression567 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_unaryExpression618 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_unaryExpression_in_unaryExpression622 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primaryExpression_in_unaryExpression638 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_primaryExpression674 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_call_in_primaryExpression692 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodDefinition_in_primaryExpression713 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_classDefinition_in_primaryExpression722 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ifBlock_in_primaryExpression732 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_whileBlock_in_primaryExpression750 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_tryBlock_in_primaryExpression765 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_OPEN_PARENT_in_primaryExpression782 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_primaryExpression790 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CLOSE_PARENT_in_primaryExpression796 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_literal822 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INTEGER_in_literal841 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FLOAT_in_literal859 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NIL_in_literal879 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_literal901 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_literal922 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_constant_in_literal942 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_instanceVariable_in_literal959 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_self_in_literal968 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SELF_in_self1002 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_AT_in_instanceVariable1036 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_NAME_in_instanceVariable1038 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_call1070 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_DOT_in_call1072 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_message_in_call1111 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_DOT_in_call1113 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_message_in_call1146 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NAME_in_message1186 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NAME_in_message1221 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_OPEN_PARENT_in_message1223 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CLOSE_PARENT_in_message1225 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NAME_in_message1235 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_OPEN_PARENT_in_message1237 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_arguments_in_message1250 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CLOSE_PARENT_in_message1261 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_arguments1336 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_COMMA_in_arguments1361 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_arguments1370 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_CONSTANT_in_constant1413 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NAME_in_assign1457 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_assign1459 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_assign1461 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASPREF_in_assign1478 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_NAME_in_assign1480 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_assign1482 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_assign1484 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_CONSTANT_in_assign1494 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_assign1496 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_assign1498 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_AT_in_assign1511 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_NAME_in_assign1513 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASSIGN_in_assign1515 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_assign1517 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DEF_in_methodDefinition1543 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_NAME_in_methodDefinition1545 = new BitSet(new long[]{0x0000040000000000L});
	public static final BitSet FOLLOW_OPEN_PARENT_in_methodDefinition1548 = new BitSet(new long[]{0x0000002000000400L});
	public static final BitSet FOLLOW_parameters_in_methodDefinition1550 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CLOSE_PARENT_in_methodDefinition1553 = new BitSet(new long[]{0x0002404000000000L});
	public static final BitSet FOLLOW_TOBE_in_methodDefinition1556 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_methodDefinition1560 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_methodDefinition1568 = new BitSet(new long[]{0x0000000000100000L});
	public static final BitSet FOLLOW_END_in_methodDefinition1574 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NAME_in_parameters1663 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_COMMA_in_parameters1694 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_NAME_in_parameters1703 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_CLASS_in_classDefinition1751 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_CONSTANT_in_classDefinition1755 = new BitSet(new long[]{0x0000404200000000L});
	public static final BitSet FOLLOW_LT_in_classDefinition1758 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_CONSTANT_in_classDefinition1762 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_classDefinition1766 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_classDefinition1774 = new BitSet(new long[]{0x0000000000100000L});
	public static final BitSet FOLLOW_END_in_classDefinition1780 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_ifBlock1828 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_ifBlock1832 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_ifBlock1834 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_ifBlock1844 = new BitSet(new long[]{0x0000000000180000L});
	public static final BitSet FOLLOW_ELSE_in_ifBlock1851 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_ifBlock1853 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_ifBlock1863 = new BitSet(new long[]{0x0000000000100000L});
	public static final BitSet FOLLOW_END_in_ifBlock1876 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHILE_in_whileBlock1924 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expression_in_whileBlock1928 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_whileBlock1930 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_whileBlock1940 = new BitSet(new long[]{0x0000000000100000L});
	public static final BitSet FOLLOW_END_in_whileBlock1946 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRY_in_tryBlock1994 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_tryBlock1996 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_tryBlock2006 = new BitSet(new long[]{0x0000000000100100L});
	public static final BitSet FOLLOW_CATCH_in_tryBlock2033 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_CONSTANT_in_tryBlock2035 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_COLON_in_tryBlock2037 = new BitSet(new long[]{0x0000002000000000L});
	public static final BitSet FOLLOW_NAME_in_tryBlock2039 = new BitSet(new long[]{0x0000404000000000L});
	public static final BitSet FOLLOW_terminator_in_tryBlock2041 = new BitSet(new long[]{0x002D25A018C0C2A0L});
	public static final BitSet FOLLOW_expressions_in_tryBlock2051 = new BitSet(new long[]{0x0000000000100100L});
	public static final BitSet FOLLOW_END_in_tryBlock2082 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_andExpression_in_synpred8_KJUScriptParser285 = new BitSet(new long[]{0x0000080000000000L});
	public static final BitSet FOLLOW_OR_in_synpred8_KJUScriptParser293 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_orExpression_in_synpred8_KJUScriptParser297 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_relationalExpression_in_synpred9_KJUScriptParser347 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_AND_in_synpred9_KJUScriptParser355 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_andExpression_in_synpred9_KJUScriptParser359 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_additiveExpression_in_synpred14_KJUScriptParser400 = new BitSet(new long[]{0x0000000243200000L});
	public static final BitSet FOLLOW_set_in_synpred14_KJUScriptParser410 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_relationalExpression_in_synpred14_KJUScriptParser430 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_synpred16_KJUScriptParser470 = new BitSet(new long[]{0x0000100400000000L});
	public static final BitSet FOLLOW_set_in_synpred16_KJUScriptParser480 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_additiveExpression_in_synpred16_KJUScriptParser488 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_unaryExpression_in_synpred19_KJUScriptParser536 = new BitSet(new long[]{0x0000001800020000L});
	public static final BitSet FOLLOW_set_in_synpred19_KJUScriptParser546 = new BitSet(new long[]{0x002D25A018C0C280L});
	public static final BitSet FOLLOW_multiplicativeExpression_in_synpred19_KJUScriptParser556 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_message_in_synpred37_KJUScriptParser1111 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_DOT_in_synpred37_KJUScriptParser1113 = new BitSet(new long[]{0x0000000000000002L});
}
