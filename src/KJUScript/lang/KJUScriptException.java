package KJUScript.lang;

/**
  Exception that can be catched from inside the runtime.
*/
public class KJUScriptException extends Exception {
  private KJUScriptClass runtimeClass;
  
  /**
    Creates a new exception from a runtime class.
    @param runtimeClass Class of the exception from whitin the language.
  */
  public KJUScriptException(KJUScriptClass runtimeClass, String message) {
    super(message);
    this.runtimeClass = runtimeClass;
  }

  public KJUScriptException(KJUScriptClass runtimeClass) {
    super();
    this.runtimeClass = runtimeClass;
  }
  
  public KJUScriptException(String runtimeClassName, String message) {
    super(message);
    setRuntimeClass(runtimeClassName);
  }
  
  /**
    Creates a new exception from the Exception runtime class.
  */
  public KJUScriptException(String message) {
    super(message);
    this.runtimeClass = KJUScriptRuntime.getExceptionClass();
  }
  
  /**
    Wrap an exception to pass it to the runtime.
  */
  public KJUScriptException(Exception inner) {
    super(inner);
    setRuntimeClass(inner.getClass().getName());
  }
  
  /**
    Returns the runtime instance (the exception inside the language) of this exception.
  */
  public KJUScriptObject getRuntimeObject() {
    KJUScriptObject instance = runtimeClass.newInstance(this);
    instance.setInstanceVariable("message", new ValueObject(getMessage()));
    return instance;
  }

  public KJUScriptClass getRuntimeClass() {
    return runtimeClass;
  }

  protected void setRuntimeClass(String name) {
    runtimeClass = KJUScriptRuntime.getExceptionClass().subclass(name);
  }
}