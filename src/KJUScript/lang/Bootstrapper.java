package KJUScript.lang;

import java.util.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern; 
import java.util.regex.*;
import java.util.ArrayList;

/**
  Bootstrapper.run() is called to initialize the runtime.
  Core classes are created and methods are added.
*/
public class Bootstrapper {
  static public Context run() {
    // Create core classes
    KJUScriptClass objectClass = new KJUScriptClass("Object");
    KJUScriptRuntime.objectClass = objectClass;
    // Each method sent or added on the root context of a script are evaled on the main object.
    KJUScriptObject main = new KJUScriptObject();
    KJUScriptRuntime.mainObject = main;
    KJUScriptClass classClass = new KJUScriptClass("Class");
    objectClass.setKJUScriptClass(classClass); // Object is a class
    classClass.setKJUScriptClass(classClass); // Class is a class
    main.setKJUScriptClass(objectClass);
    
    // Register core classes into the root context
    objectClass.setConstant("Object", objectClass);
    objectClass.setConstant("Class", classClass);
    // There is just one instance of nil, true, false, so we store those in constants.
    KJUScriptRuntime.nilObject = objectClass.newSubclass("NilClass").newInstance(null);
    KJUScriptRuntime.trueObject = objectClass.newSubclass("TrueClass").newInstance(true);
    KJUScriptRuntime.falseObject = objectClass.newSubclass("FalseClass").newInstance(false);
    KJUScriptClass stringClass = objectClass.newSubclass("String");
    KJUScriptClass numberClass = objectClass.newSubclass("Number");
    KJUScriptClass integerClass = numberClass.newSubclass("Integer");
    KJUScriptClass floatClass = numberClass.newSubclass("Float");
    KJUScriptClass exceptionClass = objectClass.newSubclass("Exception");
    exceptionClass.newSubclass("IOException");
    exceptionClass.newSubclass("TypeError");
    exceptionClass.newSubclass("MethodNotFound");
    exceptionClass.newSubclass("ArgumentError");
    exceptionClass.newSubclass("FileNotFound");
    
    // Add methods to core classes.
    
    //note, these are heavily inefficient.

    //// Object
    objectClass.addMethod("print", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        for (KJUScriptObject arg : arguments) System.out.println(arg.toJavaObject());
        return KJUScriptRuntime.getNil();
      }
    });
    objectClass.addMethod("class", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        return receiver.getKJUScriptClass();
      }
    });


    objectClass.addMethod("eval", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        Context context = new Context(receiver);
        String code = arguments[0].asString();
        return context.eval(code);
      }
    });

    objectClass.addMethod("unixTime", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        long unixTime = System.currentTimeMillis() / 1000L;
        return new ValueObject(unixTime);
      }
    });


    objectClass.addMethod("array", new Method() {
      ArrayList items = new ArrayList();
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        for (KJUScriptObject arg : arguments) items.add(arg.toJavaObject());
        return new ValueObject(items);
      }
    });

    objectClass.addMethod("re", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        Context context = new Context(receiver);
        String reg = arguments[0].asString();
        String toSearch = arguments[1].asString();
        String pre_group = arguments[2].asString();
        int group = Integer.parseInt(pre_group);

        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(toSearch);

        if (m.find( )) {
          return new ValueObject(m.group(group));
        } 
        throw new KJUScriptException("Match not found!");
      }
    });


    objectClass.addMethod("groriusOpinion", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        Context context = new Context(receiver);
        System.out.print(arguments[0].asString());
        Scanner in = new Scanner(System.in);
        return new ValueObject(in.nextLine());


      }
    });


    objectClass.addMethod("str", new Method() {
      
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        Context context = new Context(receiver);
        String code;
        try{
          code = arguments[0].asString();
        }catch(Exception e){
          try{
            code = String.valueOf(arguments[0].asInteger());
          }catch(Exception TypeError){
            try{
              code = String.valueOf(arguments[0].asFloat());
            } catch (Exception d){
              code = String.valueOf(arguments[0].asLong());

            }
            
          }
          
        }
        


        return new ValueObject(String.valueOf(code));
    }  
  });




    objectClass.addMethod("httpforKJU", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
      String url = arguments[0].asString();
      StringBuffer httpresponse = new StringBuffer();
      try{
      URL obj = new URL(url);
      HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
      con.setRequestMethod("GET");
 
    //add request header
      con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0");
      con.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
      con.setRequestProperty("Accept-Encoding", "deflate" );
      con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
      con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
      con.setRequestProperty("Connection", "keep-alive");
    
 
      BufferedReader in = new BufferedReader(
             new InputStreamReader(con.getInputStream()));
      String inputLine;
      
 
      while ((inputLine = in.readLine()) != null) {
        httpresponse.append(inputLine);
      }
      in.close();
      }catch (Exception e){
        throw new KJUScriptException(e.toString());
      }
        return new ValueObject(httpresponse.toString());
      }
      
    });


    objectClass.addMethod("require", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        Context context = new Context();
        String filename = arguments[0].asString();
        try {
          return context.eval(new FileReader(filename));
        } catch (FileNotFoundException e) {
          throw new KJUScriptException("FileNotFound", "File not found: " + filename);
        }
      }
    });
    
    //// Class
    classClass.addMethod("new", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        KJUScriptClass self = (KJUScriptClass) receiver;
        KJUScriptObject instance = self.newInstance();
        if (self.hasMethod("initialize")) instance.call("initialize", arguments);
        return instance;
      }
    });
    classClass.addMethod("name", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        KJUScriptClass self = (KJUScriptClass) receiver;
        return new ValueObject(self.getName());
      }
    });
    classClass.addMethod("superclass", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        KJUScriptClass self = (KJUScriptClass) receiver;
        return self.getSuperClass();
      }
    });

    //// Exception
    exceptionClass.addMethod("initialize", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        if (arguments.length == 1) receiver.setInstanceVariable("message", arguments[0]);
        return KJUScriptRuntime.getNil();
      }
    });
    exceptionClass.addMethod("message", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        return receiver.getInstanceVariable("message");
      }
    });
    objectClass.addMethod("raise!", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        String message = null;
        if (receiver.hasInstanceVariable("message")) message = receiver.getInstanceVariable("message").asString();
        throw new KJUScriptException(receiver.getKJUScriptClass(), message);
      }
    });
    
    //// Integer
    integerClass.addMethod("+", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return new ValueObject(receiver + argument);
      }
    });
    integerClass.addMethod("-", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return new ValueObject(receiver + argument);
      }
    });
    integerClass.addMethod("*", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return new ValueObject(receiver * argument);
      }
    });
    integerClass.addMethod("/", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return new ValueObject(receiver / argument);
      }
    });
    integerClass.addMethod("<", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return KJUScriptRuntime.toBoolean(receiver < argument);
      }
    });
    integerClass.addMethod(">", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return KJUScriptRuntime.toBoolean(receiver > argument);
      }
    });
    integerClass.addMethod("<=", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return KJUScriptRuntime.toBoolean(receiver <= argument);
      }
    });
    integerClass.addMethod(">=", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return KJUScriptRuntime.toBoolean(receiver >= argument);
      }
    });
    integerClass.addMethod("==", new OperatorMethod<Integer>() {
      public KJUScriptObject perform(Integer receiver, Integer argument) throws KJUScriptException {
        return KJUScriptRuntime.toBoolean(receiver == argument);
      }
    });
    
    //// String
    stringClass.addMethod("+", new OperatorMethod<String>() {
      public KJUScriptObject perform(String receiver, String argument) throws KJUScriptException {
        return new ValueObject(receiver + argument);
      }
    });
    stringClass.addMethod("size", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        String self = receiver.asString();
        return new ValueObject(self.length());
      }
    });
    stringClass.addMethod("substring", new Method() {
      public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
        String self = receiver.asString();
        if (arguments.length == 0) throw new ArgumentError("substring", 1, 0);
        int start = arguments[0].asInteger();
        int end = self.length();
        if (arguments.length > 1) end = arguments[1].asInteger();
        return new ValueObject(self.substring(start, end));
      }
    });
    
    // Return the root context on which everything will be evaled. By default, everything is evaled on the
    // main object.
    return new Context(main);
  }
}
