package KJUScript.lang;

import java.util.HashMap;

/**
  Class in the runtime.
  Classes store methods and constants. Each object in the runtime has a class.
*/
public class KJUScriptClass extends KJUScriptObject {
  private String name;
  private KJUScriptClass superClass;
  private HashMap<String, Method> methods;
  HashMap<String, KJUScriptObject> constants;
  
  /**
    Creates a class inheriting from superClass.
  */
  public KJUScriptClass(String name, KJUScriptClass superClass) {
    super("Class");
    this.name = name;
    this.superClass = superClass;
    methods = new HashMap<String, Method>();
    constants = new HashMap<String, KJUScriptObject>();
  }
  
  /**
    Creates a class inheriting from Object.
  */
  public KJUScriptClass(String name) {
    this(name, KJUScriptRuntime.getObjectClass());
  }
  
  public String getName() {
    return name;
  }
  
  public KJUScriptClass getSuperClass() {
    return superClass;
  }
  
  public void setConstant(String name, KJUScriptObject value) {
    constants.put(name, value);
  }

  public KJUScriptObject getConstant(String name) {
    if (constants.containsKey(name)) return constants.get(name);
    if (superClass != null) return superClass.getConstant(name);
    return KJUScriptRuntime.getNil();
  }
  
  public boolean hasConstant(String name) {
    if (constants.containsKey(name)) return true;
    if (superClass != null) return superClass.hasConstant(name);
    return false;
  }
  
  public Method lookup(String name) throws MethodNotFound {
    if (methods.containsKey(name)) return methods.get(name);
    if (superClass != null) return superClass.lookup(name);
    throw new MethodNotFound(name);
  }

  public boolean hasMethod(String name) {
    if (methods.containsKey(name)) return true;
    if (superClass != null) return superClass.hasMethod(name);
    return false;
  }
  
  public void addMethod(String name, Method method) {
    methods.put(name, method);
  }
  
  /**
    Creates a new instance of the class.
  */
  public KJUScriptObject newInstance() {
    return new KJUScriptObject(this);
  }
  
  /**
    Creates a new instance of the class, storing the value inside a ValueObject.
  */
  public KJUScriptObject newInstance(Object value) {
    return new ValueObject(this, value);
  }
  
  /**
    Creates a new subclass of this class.
  */
  public KJUScriptClass newSubclass(String name) {
    KJUScriptClass klass = new KJUScriptClass(name, this);
    KJUScriptRuntime.getObjectClass().setConstant(name, klass);
    return klass;
  }
  
  /**
    Creates or returns a subclass if it already exists.
  */
  public KJUScriptClass subclass(String name) {
    KJUScriptClass objectClass = KJUScriptRuntime.getObjectClass();
    if (objectClass.hasConstant(name)) return (KJUScriptClass) objectClass.getConstant(name);
    return newSubclass(name);
  }
  
  /**
    Returns true if klass is a subclass of this class.
  */
  public boolean isSubclass(KJUScriptClass klass) {
    if (klass == this) return true;
    if (klass.getSuperClass() == null) return false;
    if (klass.getSuperClass() == this) return true;
    return isSubclass(klass.getSuperClass());
  }
  
  @Override
  public boolean equals(Object other) {
    if (other == this) return true;
    if ( !(other instanceof KJUScriptClass) ) return false;
    return name == ((KJUScriptClass)other).getName();
  }
}
