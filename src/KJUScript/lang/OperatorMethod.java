package KJUScript.lang;

/**
  Specialized method of operators (+, -, *, /, etc.)
*/
public abstract class OperatorMethod<T> extends Method {
  @SuppressWarnings("unchecked")
  public KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException {
    T self = (T) receiver.as(ValueObject.class).getValue();
    T arg = (T) arguments[0].as(ValueObject.class).getValue();
    return perform(self, arg);
  }
  
  public abstract KJUScriptObject perform(T receiver, T argument) throws KJUScriptException;
}
