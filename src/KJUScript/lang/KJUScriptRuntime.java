package KJUScript.lang;

/**
  Language runtime. Mostly helper methods for retrieving global values.
*/
public class KJUScriptRuntime {
  static KJUScriptClass objectClass;
  static KJUScriptObject mainObject;
  static KJUScriptObject nilObject;
  static KJUScriptObject trueObject;
  static KJUScriptObject falseObject;
  
  public static KJUScriptClass getObjectClass() {
    return objectClass;
  }

  public static KJUScriptObject getMainObject() {
    return mainObject;
  }

  public static KJUScriptClass getRootClass(String name) {
    // objectClass is null when boostrapping
    return objectClass == null ? null : (KJUScriptClass) objectClass.getConstant(name);
  }

  public static KJUScriptClass getExceptionClass() {
    return getRootClass("Exception");
  }
  
  public static KJUScriptObject getNil() {
    return nilObject;
  }
  
  public static KJUScriptObject getTrue() {
    return trueObject;
  }

  public static KJUScriptObject getFalse() {
    return falseObject;
  }
  
  public static KJUScriptObject toBoolean(boolean value) {
    return value ? KJUScriptRuntime.getTrue() : KJUScriptRuntime.getFalse();
  }
}
