package KJUScript.lang;

import java.util.HashMap;
import java.util.ArrayList;

import java.io.Reader;
import java.io.StringReader;
import java.io.IOException;

import org.antlr.runtime.ANTLRReaderStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import KJUScript.lang.nodes.Node;


/**
  Evaluation context. Determines how a node will be evaluated.
  A context tracks local variables, self, and the current class under which
  methods and constants will be added.
  
  There are three different types of context:
  1) In the root of the script, self = main object, class = Object
  2) Inside a method body, self = instance of the class, class = method class
  3) Inside a class definition self = the class, class = the class
*/
public class Context {
  private KJUScriptObject currentSelf;
  private KJUScriptClass currentClass;
  private HashMap<String, KJUScriptObject> locals;
  // A context can share local variables with a parent. For example, in the
  // try block.
  private Context parent;
  
  public Context(KJUScriptObject currentSelf, KJUScriptClass currentClass, Context parent) {
    this.currentSelf = currentSelf;
    this.currentClass = currentClass;
    this.parent = parent;
    if (parent == null) {
      locals = new HashMap<String, KJUScriptObject>();
    } else {
      locals = parent.locals;
    }
  }
  
  public Context(KJUScriptObject currentSelf, KJUScriptClass currentClass) {
    this(currentSelf, currentClass, null);
  }
  
  public Context(KJUScriptObject currentSelf) {
    this(currentSelf, currentSelf.getKJUScriptClass());
  }
  
  public Context() {
    this(KJUScriptRuntime.getMainObject());
  }
  
  public KJUScriptObject getCurrentSelf() {
    return currentSelf;
  }

  public KJUScriptClass getCurrentClass() {
    return currentClass;
  }
  
  public KJUScriptObject getLocal(String name) {
    return locals.get(name);
  }
    
  public boolean hasLocal(String name) {
    return locals.containsKey(name);
  }
    
  public void setLocal(String name, KJUScriptObject value) {
    locals.put(name, value);
  }
  
  /**
    Creates a context that will share the same attributes (locals, self, class)
    as the current one.
  */
  public Context makeChildContext() {
    return new Context(currentSelf, currentClass, this);
  }
  
  /**
    Parse and evaluate the content red from the reader (eg.: FileReader, StringReader).
  */
  public KJUScriptObject eval(Reader reader) throws KJUScriptException {
    try {
      KJUScriptLexer lexer = new KJUScriptLexer(new ANTLRReaderStream(reader));
      KJUScriptParser parser = new KJUScriptParser(new CommonTokenStream(lexer));
      Node node = parser.parse();
      if (node == null) return KJUScriptRuntime.getNil();
      return node.eval(this);
    } catch (KJUScriptException e) {
      throw e;
    } catch (Exception e) {
      throw new KJUScriptException(e);
    }
  }
  
  public KJUScriptObject eval(String code) throws KJUScriptException {
    return eval(new StringReader(code));
  }
}
