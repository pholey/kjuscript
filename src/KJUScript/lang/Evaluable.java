package KJUScript.lang;

import KJUScript.lang.nodes.Node;

/**
  Anything that can be evaluated inside a context must implement this interface.
*/
public interface Evaluable {
  KJUScriptObject eval(Context context) throws KJUScriptException;
}
