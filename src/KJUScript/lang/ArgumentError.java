package KJUScript.lang;

/**
  Exception thrown when a unknown method is called.
*/
public class ArgumentError extends KJUScriptException {
  public ArgumentError(String method, int expected, int actual) {
    super("Expected " + expected + " arguments for " + method + ", got " + actual);
    setRuntimeClass("ArgumentError");
  }
}