// $ANTLR 3.x KJUScriptLexer.g 2013-12-02 19:44:37
 package KJUScript.lang; 

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class KJUScriptLexer extends Lexer {
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ASPREF=5;
	public static final int ASSIGN=6;
	public static final int AT=7;
	public static final int CATCH=8;
	public static final int CLASS=9;
	public static final int CLOSE_PARENT=10;
	public static final int COLON=11;
	public static final int COMMA=12;
	public static final int COMMENT=13;
	public static final int CONSTANT=14;
	public static final int DEF=15;
	public static final int DIGIT=16;
	public static final int DIV=17;
	public static final int DOT=18;
	public static final int ELSE=19;
	public static final int END=20;
	public static final int EQ=21;
	public static final int FALSE=22;
	public static final int FLOAT=23;
	public static final int GE=24;
	public static final int GT=25;
	public static final int ID_CHAR=26;
	public static final int IF=27;
	public static final int INTEGER=28;
	public static final int LAM=29;
	public static final int LE=30;
	public static final int LETTER=31;
	public static final int LOWER=32;
	public static final int LT=33;
	public static final int MINUS=34;
	public static final int MOD=35;
	public static final int MUL=36;
	public static final int NAME=37;
	public static final int NEWLINE=38;
	public static final int NIL=39;
	public static final int NOT=40;
	public static final int NUMBER=41;
	public static final int OPEN_PARENT=42;
	public static final int OR=43;
	public static final int PLUS=44;
	public static final int SELF=45;
	public static final int SEMICOLON=46;
	public static final int SPACE=47;
	public static final int STRING=48;
	public static final int TOBE=49;
	public static final int TRUE=50;
	public static final int TRY=51;
	public static final int UPPER=52;
	public static final int WHILE=53;
	public static final int WHITESPACE=54;

	  boolean methodMode = false; // true if we're waiting for a method name

	  public Token nextToken() {
	    Token t = super.nextToken();
	    // DEBUG Uncomment to output tokens
	    // System.out.println("TOKEN> " + t);
	    return t;
	  }
	  
	  public boolean isNum(int c) {
	    return c>='0' && c<='9';
	  }
	  
	  @Override
	  public void reportError(RecognitionException e) {
	    throw new RuntimeException(e);
	  }


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public KJUScriptLexer() {} 
	public KJUScriptLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public KJUScriptLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "KJUScriptLexer.g"; }

	// $ANTLR start "CLASS"
	public final void mCLASS() throws RecognitionException {
		try {
			int _type = CLASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:29:6: ({...}? => 'The Grorius Reader' )
			// KJUScriptLexer.g:29:21: {...}? => 'The Grorius Reader'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "CLASS", "!methodMode");
			}
			match("The Grorius Reader"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLASS"

	// $ANTLR start "DEF"
	public final void mDEF() throws RecognitionException {
		try {
			int _type = DEF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:30:4: ({...}? => 'KJU commands' )
			// KJUScriptLexer.g:30:21: {...}? => 'KJU commands'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "DEF", "!methodMode");
			}
			match("KJU commands"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DEF"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:31:3: ({...}? => 'when KJU senses' )
			// KJUScriptLexer.g:31:21: {...}? => 'when KJU senses'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "IF", "!methodMode");
			}
			match("when KJU senses"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:32:5: ({...}? => 'when KJU is tired of sensing:' )
			// KJUScriptLexer.g:32:21: {...}? => 'when KJU is tired of sensing:'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "ELSE", "!methodMode");
			}
			match("when KJU is tired of sensing:"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "WHILE"
	public final void mWHILE() throws RecognitionException {
		try {
			int _type = WHILE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:33:6: ({...}? => 'while' )
			// KJUScriptLexer.g:33:21: {...}? => 'while'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "WHILE", "!methodMode");
			}
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHILE"

	// $ANTLR start "TRY"
	public final void mTRY() throws RecognitionException {
		try {
			int _type = TRY;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:34:4: ({...}? => 'KJUspaceprogram' )
			// KJUScriptLexer.g:34:21: {...}? => 'KJUspaceprogram'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "TRY", "!methodMode");
			}
			match("KJUspaceprogram"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRY"

	// $ANTLR start "CATCH"
	public final void mCATCH() throws RecognitionException {
		try {
			int _type = CATCH;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:35:6: ({...}? => 'IMPOSSIBRU' )
			// KJUScriptLexer.g:35:21: {...}? => 'IMPOSSIBRU'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "CATCH", "!methodMode");
			}
			match("IMPOSSIBRU"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CATCH"

	// $ANTLR start "END"
	public final void mEND() throws RecognitionException {
		try {
			int _type = END;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:36:4: ({...}? => 'shamefur end' )
			// KJUScriptLexer.g:36:21: {...}? => 'shamefur end'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "END", "!methodMode");
			}
			match("shamefur end"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "END"

	// $ANTLR start "SELF"
	public final void mSELF() throws RecognitionException {
		try {
			int _type = SELF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:37:5: ({...}? => 'pawn' )
			// KJUScriptLexer.g:37:21: {...}? => 'pawn'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "SELF", "!methodMode");
			}
			match("pawn"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SELF"

	// $ANTLR start "NIL"
	public final void mNIL() throws RecognitionException {
		try {
			int _type = NIL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:38:4: ({...}? => 'INVISIBRU' )
			// KJUScriptLexer.g:38:21: {...}? => 'INVISIBRU'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "NIL", "!methodMode");
			}
			match("INVISIBRU"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NIL"

	// $ANTLR start "TRUE"
	public final void mTRUE() throws RecognitionException {
		try {
			int _type = TRUE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:39:5: ({...}? => 'KJUapproves' )
			// KJUScriptLexer.g:39:21: {...}? => 'KJUapproves'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "TRUE", "!methodMode");
			}
			match("KJUapproves"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TRUE"

	// $ANTLR start "FALSE"
	public final void mFALSE() throws RecognitionException {
		try {
			int _type = FALSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:40:6: ({...}? => 'KJUdeemsNonexistant' )
			// KJUScriptLexer.g:40:21: {...}? => 'KJUdeemsNonexistant'
			{
			if ( !((!methodMode)) ) {
				throw new FailedPredicateException(input, "FALSE", "!methodMode");
			}
			match("KJUdeemsNonexistant"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FALSE"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			// KJUScriptLexer.g:44:17: ()
			// KJUScriptLexer.g:44:18: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			// KJUScriptLexer.g:45:15: ()
			// KJUScriptLexer.g:45:16: 
			{
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOAT"

	// $ANTLR start "NUMBER"
	public final void mNUMBER() throws RecognitionException {
		try {
			int _type = NUMBER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:46:7: ( ( '-' )? ( DIGIT )+ ({...}? => '.' ( DIGIT )+ |) )
			// KJUScriptLexer.g:46:21: ( '-' )? ( DIGIT )+ ({...}? => '.' ( DIGIT )+ |)
			{
			// KJUScriptLexer.g:46:21: ( '-' )?
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='-') ) {
				alt1=1;
			}
			switch (alt1) {
				case 1 :
					// KJUScriptLexer.g:46:21: '-'
					{
					match('-'); 
					}
					break;

			}

			// KJUScriptLexer.g:46:26: ( DIGIT )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			// KJUScriptLexer.g:48:23: ({...}? => '.' ( DIGIT )+ |)
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0=='.') && ((isNum(input.LA(2))))) {
				alt4=1;
			}

			switch (alt4) {
				case 1 :
					// KJUScriptLexer.g:48:25: {...}? => '.' ( DIGIT )+
					{
					if ( !((isNum(input.LA(2)))) ) {
						throw new FailedPredicateException(input, "NUMBER", "isNum(input.LA(2))");
					}
					match('.'); 
					// KJUScriptLexer.g:48:53: ( DIGIT )+
					int cnt3=0;
					loop3:
					while (true) {
						int alt3=2;
						int LA3_0 = input.LA(1);
						if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
							alt3=1;
						}

						switch (alt3) {
						case 1 :
							// KJUScriptLexer.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt3 >= 1 ) break loop3;
							EarlyExitException eee = new EarlyExitException(3, input);
							throw eee;
						}
						cnt3++;
					}

					 _type = FLOAT; 
					}
					break;
				case 2 :
					// KJUScriptLexer.g:49:61: 
					{
					 _type = INTEGER; 
					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NUMBER"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:51:7: ( '\"' (~ ( '\\\\' | '\"' ) )* '\"' )
			// KJUScriptLexer.g:51:21: '\"' (~ ( '\\\\' | '\"' ) )* '\"'
			{
			match('\"'); 
			// KJUScriptLexer.g:51:25: (~ ( '\\\\' | '\"' ) )*
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( ((LA5_0 >= '\u0000' && LA5_0 <= '!')||(LA5_0 >= '#' && LA5_0 <= '[')||(LA5_0 >= ']' && LA5_0 <= '\uFFFF')) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop5;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "NAME"
	public final void mNAME() throws RecognitionException {
		try {
			int _type = NAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:52:5: ( ( LOWER | '_' ) ( ID_CHAR )* ( '!' | '?' )? )
			// KJUScriptLexer.g:52:21: ( LOWER | '_' ) ( ID_CHAR )* ( '!' | '?' )?
			{
			if ( input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// KJUScriptLexer.g:52:35: ( ID_CHAR )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( ((LA6_0 >= '0' && LA6_0 <= '9')||(LA6_0 >= 'A' && LA6_0 <= 'Z')||LA6_0=='_'||(LA6_0 >= 'a' && LA6_0 <= 'z')) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop6;
				}
			}

			// KJUScriptLexer.g:52:44: ( '!' | '?' )?
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0=='!'||LA7_0=='?') ) {
				alt7=1;
			}
			switch (alt7) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( input.LA(1)=='!'||input.LA(1)=='?' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			 methodMode = false; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NAME"

	// $ANTLR start "CONSTANT"
	public final void mCONSTANT() throws RecognitionException {
		try {
			int _type = CONSTANT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:53:9: ( UPPER ( ID_CHAR )* )
			// KJUScriptLexer.g:53:21: UPPER ( ID_CHAR )*
			{
			mUPPER(); 

			// KJUScriptLexer.g:53:27: ( ID_CHAR )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= '0' && LA8_0 <= '9')||(LA8_0 >= 'A' && LA8_0 <= 'Z')||LA8_0=='_'||(LA8_0 >= 'a' && LA8_0 <= 'z')) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop8;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CONSTANT"

	// $ANTLR start "SEMICOLON"
	public final void mSEMICOLON() throws RecognitionException {
		try {
			int _type = SEMICOLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:56:10: ( ';' )
			// KJUScriptLexer.g:56:21: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SEMICOLON"

	// $ANTLR start "COLON"
	public final void mCOLON() throws RecognitionException {
		try {
			int _type = COLON;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:57:6: ( ':' )
			// KJUScriptLexer.g:57:21: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COLON"

	// $ANTLR start "DOT"
	public final void mDOT() throws RecognitionException {
		try {
			int _type = DOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:58:4: ( '.' )
			// KJUScriptLexer.g:58:21: '.'
			{
			match('.'); 
			 methodMode = true; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOT"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:59:6: ( ',' )
			// KJUScriptLexer.g:59:21: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "OPEN_PARENT"
	public final void mOPEN_PARENT() throws RecognitionException {
		try {
			int _type = OPEN_PARENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:60:12: ( '(' )
			// KJUScriptLexer.g:60:21: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OPEN_PARENT"

	// $ANTLR start "CLOSE_PARENT"
	public final void mCLOSE_PARENT() throws RecognitionException {
		try {
			int _type = CLOSE_PARENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:61:13: ( ')' )
			// KJUScriptLexer.g:61:21: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CLOSE_PARENT"

	// $ANTLR start "AT"
	public final void mAT() throws RecognitionException {
		try {
			int _type = AT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:62:3: ( '@' )
			// KJUScriptLexer.g:62:21: '@'
			{
			match('@'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AT"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:63:3: ( '==' )
			// KJUScriptLexer.g:63:21: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "LE"
	public final void mLE() throws RecognitionException {
		try {
			int _type = LE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:64:3: ( '<=' )
			// KJUScriptLexer.g:64:21: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LE"

	// $ANTLR start "GE"
	public final void mGE() throws RecognitionException {
		try {
			int _type = GE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:65:3: ( '>=' )
			// KJUScriptLexer.g:65:21: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GE"

	// $ANTLR start "LT"
	public final void mLT() throws RecognitionException {
		try {
			int _type = LT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:66:3: ( '<' )
			// KJUScriptLexer.g:66:21: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LT"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			int _type = GT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:67:3: ( '>' )
			// KJUScriptLexer.g:67:21: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GT"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:68:5: ( '+' )
			// KJUScriptLexer.g:68:21: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:69:6: ( '-' )
			// KJUScriptLexer.g:69:21: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:70:4: ( '*' )
			// KJUScriptLexer.g:70:21: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:71:4: ( '/' )
			// KJUScriptLexer.g:71:21: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "MOD"
	public final void mMOD() throws RecognitionException {
		try {
			int _type = MOD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:72:4: ( '%' )
			// KJUScriptLexer.g:72:21: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MOD"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:73:4: ( '&&' )
			// KJUScriptLexer.g:73:21: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:74:3: ( '||' )
			// KJUScriptLexer.g:74:21: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:75:4: ( '!' )
			// KJUScriptLexer.g:75:21: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "ASSIGN"
	public final void mASSIGN() throws RecognitionException {
		try {
			int _type = ASSIGN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:76:7: ( 'is now' )
			// KJUScriptLexer.g:76:21: 'is now'
			{
			match("is now"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASSIGN"

	// $ANTLR start "LAM"
	public final void mLAM() throws RecognitionException {
		try {
			int _type = LAM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:77:4: ( '`' )
			// KJUScriptLexer.g:77:22: '`'
			{
			match('`'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LAM"

	// $ANTLR start "ASPREF"
	public final void mASPREF() throws RecognitionException {
		try {
			int _type = ASPREF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:83:7: ( 'KJU says' )
			// KJUScriptLexer.g:83:21: 'KJU says'
			{
			match("KJU says"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASPREF"

	// $ANTLR start "TOBE"
	public final void mTOBE() throws RecognitionException {
		try {
			int _type = TOBE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:84:5: ( 'to be:' )
			// KJUScriptLexer.g:84:21: 'to be:'
			{
			match("to be:"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TOBE"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:89:8: ( '#' (~ ( '\\r' | '\\n' ) )* ( NEWLINE | EOF ) )
			// KJUScriptLexer.g:89:21: '#' (~ ( '\\r' | '\\n' ) )* ( NEWLINE | EOF )
			{
			match('#'); 
			// KJUScriptLexer.g:89:25: (~ ( '\\r' | '\\n' ) )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( ((LA9_0 >= '\u0000' && LA9_0 <= '\t')||(LA9_0 >= '\u000B' && LA9_0 <= '\f')||(LA9_0 >= '\u000E' && LA9_0 <= '\uFFFF')) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop9;
				}
			}

			// KJUScriptLexer.g:89:41: ( NEWLINE | EOF )
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0=='\n'||LA10_0=='\r') ) {
				alt10=1;
			}

			else {
				alt10=2;
			}

			switch (alt10) {
				case 1 :
					// KJUScriptLexer.g:89:42: NEWLINE
					{
					mNEWLINE(); 

					}
					break;
				case 2 :
					// KJUScriptLexer.g:89:52: EOF
					{
					match(EOF); 

					}
					break;

			}

			 skip(); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "NEWLINE"
	public final void mNEWLINE() throws RecognitionException {
		try {
			int _type = NEWLINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:91:8: ( ( '\\r' )? '\\n' )
			// KJUScriptLexer.g:91:21: ( '\\r' )? '\\n'
			{
			// KJUScriptLexer.g:91:21: ( '\\r' )?
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0=='\r') ) {
				alt11=1;
			}
			switch (alt11) {
				case 1 :
					// KJUScriptLexer.g:91:21: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEWLINE"

	// $ANTLR start "WHITESPACE"
	public final void mWHITESPACE() throws RecognitionException {
		try {
			int _type = WHITESPACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// KJUScriptLexer.g:92:11: ( ( SPACE )+ )
			// KJUScriptLexer.g:92:21: ( SPACE )+
			{
			// KJUScriptLexer.g:92:21: ( SPACE )+
			int cnt12=0;
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( (LA12_0=='\t'||LA12_0==' ') ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// KJUScriptLexer.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt12 >= 1 ) break loop12;
					EarlyExitException eee = new EarlyExitException(12, input);
					throw eee;
				}
				cnt12++;
			}

			 _channel = HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WHITESPACE"

	// $ANTLR start "LETTER"
	public final void mLETTER() throws RecognitionException {
		try {
			// KJUScriptLexer.g:94:16: ( LOWER | UPPER )
			// KJUScriptLexer.g:
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LETTER"

	// $ANTLR start "ID_CHAR"
	public final void mID_CHAR() throws RecognitionException {
		try {
			// KJUScriptLexer.g:95:17: ( LETTER | DIGIT | '_' )
			// KJUScriptLexer.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID_CHAR"

	// $ANTLR start "LOWER"
	public final void mLOWER() throws RecognitionException {
		try {
			// KJUScriptLexer.g:96:15: ( 'a' .. 'z' )
			// KJUScriptLexer.g:
			{
			if ( (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LOWER"

	// $ANTLR start "UPPER"
	public final void mUPPER() throws RecognitionException {
		try {
			// KJUScriptLexer.g:97:15: ( 'A' .. 'Z' )
			// KJUScriptLexer.g:
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UPPER"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// KJUScriptLexer.g:98:15: ( '0' .. '9' )
			// KJUScriptLexer.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "SPACE"
	public final void mSPACE() throws RecognitionException {
		try {
			// KJUScriptLexer.g:99:15: ( ' ' | '\\t' )
			// KJUScriptLexer.g:
			{
			if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SPACE"

	@Override
	public void mTokens() throws RecognitionException {
		// KJUScriptLexer.g:1:8: ( CLASS | DEF | IF | ELSE | WHILE | TRY | CATCH | END | SELF | NIL | TRUE | FALSE | NUMBER | STRING | NAME | CONSTANT | SEMICOLON | COLON | DOT | COMMA | OPEN_PARENT | CLOSE_PARENT | AT | EQ | LE | GE | LT | GT | PLUS | MINUS | MUL | DIV | MOD | AND | OR | NOT | ASSIGN | LAM | ASPREF | TOBE | COMMENT | NEWLINE | WHITESPACE )
		int alt13=43;
		alt13 = dfa13.predict(input);
		switch (alt13) {
			case 1 :
				// KJUScriptLexer.g:1:10: CLASS
				{
				mCLASS(); 

				}
				break;
			case 2 :
				// KJUScriptLexer.g:1:16: DEF
				{
				mDEF(); 

				}
				break;
			case 3 :
				// KJUScriptLexer.g:1:20: IF
				{
				mIF(); 

				}
				break;
			case 4 :
				// KJUScriptLexer.g:1:23: ELSE
				{
				mELSE(); 

				}
				break;
			case 5 :
				// KJUScriptLexer.g:1:28: WHILE
				{
				mWHILE(); 

				}
				break;
			case 6 :
				// KJUScriptLexer.g:1:34: TRY
				{
				mTRY(); 

				}
				break;
			case 7 :
				// KJUScriptLexer.g:1:38: CATCH
				{
				mCATCH(); 

				}
				break;
			case 8 :
				// KJUScriptLexer.g:1:44: END
				{
				mEND(); 

				}
				break;
			case 9 :
				// KJUScriptLexer.g:1:48: SELF
				{
				mSELF(); 

				}
				break;
			case 10 :
				// KJUScriptLexer.g:1:53: NIL
				{
				mNIL(); 

				}
				break;
			case 11 :
				// KJUScriptLexer.g:1:57: TRUE
				{
				mTRUE(); 

				}
				break;
			case 12 :
				// KJUScriptLexer.g:1:62: FALSE
				{
				mFALSE(); 

				}
				break;
			case 13 :
				// KJUScriptLexer.g:1:68: NUMBER
				{
				mNUMBER(); 

				}
				break;
			case 14 :
				// KJUScriptLexer.g:1:75: STRING
				{
				mSTRING(); 

				}
				break;
			case 15 :
				// KJUScriptLexer.g:1:82: NAME
				{
				mNAME(); 

				}
				break;
			case 16 :
				// KJUScriptLexer.g:1:87: CONSTANT
				{
				mCONSTANT(); 

				}
				break;
			case 17 :
				// KJUScriptLexer.g:1:96: SEMICOLON
				{
				mSEMICOLON(); 

				}
				break;
			case 18 :
				// KJUScriptLexer.g:1:106: COLON
				{
				mCOLON(); 

				}
				break;
			case 19 :
				// KJUScriptLexer.g:1:112: DOT
				{
				mDOT(); 

				}
				break;
			case 20 :
				// KJUScriptLexer.g:1:116: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 21 :
				// KJUScriptLexer.g:1:122: OPEN_PARENT
				{
				mOPEN_PARENT(); 

				}
				break;
			case 22 :
				// KJUScriptLexer.g:1:134: CLOSE_PARENT
				{
				mCLOSE_PARENT(); 

				}
				break;
			case 23 :
				// KJUScriptLexer.g:1:147: AT
				{
				mAT(); 

				}
				break;
			case 24 :
				// KJUScriptLexer.g:1:150: EQ
				{
				mEQ(); 

				}
				break;
			case 25 :
				// KJUScriptLexer.g:1:153: LE
				{
				mLE(); 

				}
				break;
			case 26 :
				// KJUScriptLexer.g:1:156: GE
				{
				mGE(); 

				}
				break;
			case 27 :
				// KJUScriptLexer.g:1:159: LT
				{
				mLT(); 

				}
				break;
			case 28 :
				// KJUScriptLexer.g:1:162: GT
				{
				mGT(); 

				}
				break;
			case 29 :
				// KJUScriptLexer.g:1:165: PLUS
				{
				mPLUS(); 

				}
				break;
			case 30 :
				// KJUScriptLexer.g:1:170: MINUS
				{
				mMINUS(); 

				}
				break;
			case 31 :
				// KJUScriptLexer.g:1:176: MUL
				{
				mMUL(); 

				}
				break;
			case 32 :
				// KJUScriptLexer.g:1:180: DIV
				{
				mDIV(); 

				}
				break;
			case 33 :
				// KJUScriptLexer.g:1:184: MOD
				{
				mMOD(); 

				}
				break;
			case 34 :
				// KJUScriptLexer.g:1:188: AND
				{
				mAND(); 

				}
				break;
			case 35 :
				// KJUScriptLexer.g:1:192: OR
				{
				mOR(); 

				}
				break;
			case 36 :
				// KJUScriptLexer.g:1:195: NOT
				{
				mNOT(); 

				}
				break;
			case 37 :
				// KJUScriptLexer.g:1:199: ASSIGN
				{
				mASSIGN(); 

				}
				break;
			case 38 :
				// KJUScriptLexer.g:1:206: LAM
				{
				mLAM(); 

				}
				break;
			case 39 :
				// KJUScriptLexer.g:1:210: ASPREF
				{
				mASPREF(); 

				}
				break;
			case 40 :
				// KJUScriptLexer.g:1:217: TOBE
				{
				mTOBE(); 

				}
				break;
			case 41 :
				// KJUScriptLexer.g:1:222: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 42 :
				// KJUScriptLexer.g:1:230: NEWLINE
				{
				mNEWLINE(); 

				}
				break;
			case 43 :
				// KJUScriptLexer.g:1:238: WHITESPACE
				{
				mWHITESPACE(); 

				}
				break;

		}
	}


	protected DFA13 dfa13 = new DFA13(this);
	static final String DFA13_eotS =
		"\1\uffff\2\13\1\37\1\13\2\37\1\52\2\uffff\1\37\11\uffff\1\55\1\57\7\uffff"+
		"\1\37\5\uffff\2\13\1\37\2\13\2\37\1\uffff\1\37\4\uffff\1\37\2\13\2\37"+
		"\2\13\2\37\4\uffff\3\13\2\37\2\13\1\37\1\120\2\uffff\3\13\1\uffff\1\125"+
		"\2\13\1\37\1\uffff\3\13\2\uffff\2\13\1\37\1\uffff\3\13\2\uffff\2\13\1"+
		"\37\3\13\1\uffff\2\13\1\37\3\13\1\uffff\1\13\1\166\1\uffff\3\13\2\uffff"+
		"\1\172\1\uffff\1\13\1\175\1\13\2\uffff\1\13\1\uffff\1\13\1\uffff\1\13"+
		"\1\uffff\3\13\1\u0087\1\13\1\uffff\1\13\1\uffff\2\13\1\u008d\2\uffff";
	static final String DFA13_eofS =
		"\u008f\uffff";
	static final String DFA13_minS =
		"\1\11\1\150\1\112\1\150\1\115\1\150\1\141\1\60\2\uffff\1\163\11\uffff"+
		"\2\75\7\uffff\1\157\5\uffff\1\145\1\125\1\145\1\120\1\126\1\141\1\167"+
		"\1\uffff\1\40\4\uffff\3\40\1\156\1\154\1\117\1\111\1\155\1\156\3\uffff"+
		"\1\143\2\160\1\145\1\40\1\145\2\123\1\145\1\41\2\uffff\1\141\1\160\1\145"+
		"\1\113\1\41\1\123\1\111\1\146\1\0\1\143\1\162\1\155\1\112\1\0\1\111\1"+
		"\102\1\165\1\uffff\1\145\1\157\1\163\1\125\1\uffff\1\102\1\122\1\162\1"+
		"\160\1\166\1\116\1\40\1\122\1\125\1\40\1\162\1\145\1\157\1\151\1\125\1"+
		"\60\1\uffff\1\157\1\163\1\156\2\uffff\1\60\1\0\1\147\1\60\1\145\1\0\1"+
		"\uffff\1\162\1\0\1\170\1\uffff\1\141\1\uffff\1\151\1\155\1\163\1\60\1"+
		"\164\1\0\1\141\1\uffff\1\156\1\164\1\60\1\0\1\uffff";
	static final String DFA13_maxS =
		"\1\174\1\150\1\112\1\150\1\116\1\150\1\141\1\71\2\uffff\1\163\11\uffff"+
		"\2\75\7\uffff\1\157\5\uffff\1\145\1\125\1\151\1\120\1\126\1\141\1\167"+
		"\1\uffff\1\40\4\uffff\2\40\1\163\1\156\1\154\1\117\1\111\1\155\1\156\3"+
		"\uffff\1\163\2\160\1\145\1\40\1\145\2\123\1\145\1\172\2\uffff\1\141\1"+
		"\160\1\145\1\113\1\172\1\123\1\111\1\146\1\0\1\143\1\162\1\155\1\112\1"+
		"\0\1\111\1\102\1\165\1\uffff\1\145\1\157\1\163\1\125\1\uffff\1\102\1\122"+
		"\1\162\1\160\1\166\1\116\1\40\1\122\1\125\1\40\1\162\1\145\1\157\1\163"+
		"\1\125\1\172\1\uffff\1\157\1\163\1\156\2\uffff\1\172\1\0\1\147\1\172\1"+
		"\145\1\0\1\uffff\1\162\1\0\1\170\1\uffff\1\141\1\uffff\1\151\1\155\1\163"+
		"\1\172\1\164\1\0\1\141\1\uffff\1\156\1\164\1\172\1\0\1\uffff";
	static final String DFA13_acceptS =
		"\10\uffff\1\15\1\16\1\uffff\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\27\1"+
		"\30\2\uffff\1\35\1\37\1\40\1\41\1\42\1\43\1\44\1\uffff\1\46\1\17\1\51"+
		"\1\52\1\53\7\uffff\1\36\1\uffff\1\31\1\33\1\32\1\34\11\uffff\1\45\1\50"+
		"\1\1\12\uffff\1\2\1\47\21\uffff\1\11\4\uffff\1\5\20\uffff\1\10\3\uffff"+
		"\1\3\1\4\6\uffff\1\12\3\uffff\1\7\1\uffff\1\13\7\uffff\1\6\4\uffff\1\14";
	static final String DFA13_specialS =
		"\61\uffff\1\1\12\uffff\1\5\3\uffff\1\10\12\uffff\1\2\4\uffff\1\14\3\uffff"+
		"\1\0\1\11\7\uffff\1\6\7\uffff\1\4\2\uffff\1\7\3\uffff\1\3\11\uffff\1\16"+
		"\3\uffff\1\12\2\uffff\1\15\11\uffff\1\13\5\uffff\1\17\1\uffff}>";
	static final String[] DFA13_transitionS = {
			"\1\42\1\41\2\uffff\1\41\22\uffff\1\42\1\34\1\11\1\40\1\uffff\1\31\1\32"+
			"\1\uffff\1\20\1\21\1\27\1\26\1\17\1\7\1\16\1\30\12\10\1\15\1\14\1\24"+
			"\1\23\1\25\1\uffff\1\22\10\13\1\4\1\13\1\2\10\13\1\1\6\13\4\uffff\1\37"+
			"\1\36\10\37\1\12\6\37\1\6\2\37\1\5\1\35\2\37\1\3\3\37\1\uffff\1\33",
			"\1\43",
			"\1\44",
			"\1\45",
			"\1\46\1\47",
			"\1\50",
			"\1\51",
			"\12\10",
			"",
			"",
			"\1\53",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\54",
			"\1\56",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\60",
			"",
			"",
			"",
			"",
			"",
			"\1\61",
			"\1\62",
			"\1\63\3\uffff\1\64",
			"\1\65",
			"\1\66",
			"\1\67",
			"\1\70",
			"",
			"\1\71",
			"",
			"",
			"",
			"",
			"\1\72",
			"\1\73",
			"\1\74\100\uffff\1\76\2\uffff\1\77\16\uffff\1\75",
			"\1\100",
			"\1\101",
			"\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"",
			"",
			"",
			"\1\106\17\uffff\1\107",
			"\1\110",
			"\1\111",
			"\1\112",
			"\1\113",
			"\1\114",
			"\1\115",
			"\1\116",
			"\1\117",
			"\1\37\16\uffff\12\37\5\uffff\1\37\1\uffff\32\37\4\uffff\1\37\1\uffff"+
			"\32\37",
			"",
			"",
			"\1\121",
			"\1\122",
			"\1\123",
			"\1\124",
			"\1\37\16\uffff\12\37\5\uffff\1\37\1\uffff\32\37\4\uffff\1\37\1\uffff"+
			"\32\37",
			"\1\126",
			"\1\127",
			"\1\130",
			"\1\uffff",
			"\1\132",
			"\1\133",
			"\1\134",
			"\1\135",
			"\1\uffff",
			"\1\137",
			"\1\140",
			"\1\141",
			"",
			"\1\142",
			"\1\143",
			"\1\144",
			"\1\145",
			"",
			"\1\146",
			"\1\147",
			"\1\150",
			"\1\151",
			"\1\152",
			"\1\153",
			"\1\154",
			"\1\155",
			"\1\156",
			"\1\157",
			"\1\160",
			"\1\161",
			"\1\162",
			"\1\164\11\uffff\1\163",
			"\1\165",
			"\12\13\7\uffff\32\13\4\uffff\1\13\1\uffff\32\13",
			"",
			"\1\167",
			"\1\170",
			"\1\171",
			"",
			"",
			"\12\13\7\uffff\32\13\4\uffff\1\13\1\uffff\32\13",
			"\1\uffff",
			"\1\174",
			"\12\13\7\uffff\32\13\4\uffff\1\13\1\uffff\32\13",
			"\1\176",
			"\1\uffff",
			"",
			"\1\u0080",
			"\1\uffff",
			"\1\u0082",
			"",
			"\1\u0083",
			"",
			"\1\u0084",
			"\1\u0085",
			"\1\u0086",
			"\12\13\7\uffff\32\13\4\uffff\1\13\1\uffff\32\13",
			"\1\u0088",
			"\1\uffff",
			"\1\u008a",
			"",
			"\1\u008b",
			"\1\u008c",
			"\12\13\7\uffff\32\13\4\uffff\1\13\1\uffff\32\13",
			"\1\uffff",
			""
	};

	static final short[] DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
	static final short[] DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
	static final char[] DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
	static final char[] DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
	static final short[] DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
	static final short[] DFA13_special = DFA.unpackEncodedString(DFA13_specialS);
	static final short[][] DFA13_transition;

	static {
		int numStates = DFA13_transitionS.length;
		DFA13_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
		}
	}

	protected class DFA13 extends DFA {

		public DFA13(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 13;
			this.eot = DFA13_eot;
			this.eof = DFA13_eof;
			this.min = DFA13_min;
			this.max = DFA13_max;
			this.accept = DFA13_accept;
			this.special = DFA13_special;
			this.transition = DFA13_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( CLASS | DEF | IF | ELSE | WHILE | TRY | CATCH | END | SELF | NIL | TRUE | FALSE | NUMBER | STRING | NAME | CONSTANT | SEMICOLON | COLON | DOT | COMMA | OPEN_PARENT | CLOSE_PARENT | AT | EQ | LE | GE | LT | GT | PLUS | MINUS | MUL | DIV | MOD | AND | OR | NOT | ASSIGN | LAM | ASPREF | TOBE | COMMENT | NEWLINE | WHITESPACE );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA13_84 = input.LA(1);
						 
						int index13_84 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_84=='J') && ((!methodMode))) {s = 93;}
						 
						input.seek(index13_84);
						if ( s>=0 ) return s;
						break;

					case 1 : 
						int LA13_49 = input.LA(1);
						 
						int index13_49 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_49==' ') && ((!methodMode))) {s = 59;}
						else s = 11;
						 
						input.seek(index13_49);
						if ( s>=0 ) return s;
						break;

					case 2 : 
						int LA13_75 = input.LA(1);
						 
						int index13_75 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_75=='K') && ((!methodMode))) {s = 84;}
						 
						input.seek(index13_75);
						if ( s>=0 ) return s;
						break;

					case 3 : 
						int LA13_108 = input.LA(1);
						 
						int index13_108 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_108=='s') && ((!methodMode))) {s = 115;}
						else if ( (LA13_108=='i') && ((!methodMode))) {s = 116;}
						 
						input.seek(index13_108);
						if ( s>=0 ) return s;
						break;

					case 4 : 
						int LA13_101 = input.LA(1);
						 
						int index13_101 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_101==' ') && ((!methodMode))) {s = 108;}
						 
						input.seek(index13_101);
						if ( s>=0 ) return s;
						break;

					case 5 : 
						int LA13_60 = input.LA(1);
						 
						int index13_60 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_60=='c') && ((!methodMode))) {s = 70;}
						else if ( (LA13_60=='s') ) {s = 71;}
						 
						input.seek(index13_60);
						if ( s>=0 ) return s;
						break;

					case 6 : 
						int LA13_93 = input.LA(1);
						 
						int index13_93 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_93=='U') && ((!methodMode))) {s = 101;}
						 
						input.seek(index13_93);
						if ( s>=0 ) return s;
						break;

					case 7 : 
						int LA13_104 = input.LA(1);
						 
						int index13_104 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_104==' ') && ((!methodMode))) {s = 111;}
						else s = 31;
						 
						input.seek(index13_104);
						if ( s>=0 ) return s;
						break;

					case 8 : 
						int LA13_64 = input.LA(1);
						 
						int index13_64 = input.index();
						input.rewind();
						s = -1;
						if ( (LA13_64==' ') && ((!methodMode))) {s = 75;}
						else s = 31;
						 
						input.seek(index13_64);
						if ( s>=0 ) return s;
						break;

					case 9 : 
						int LA13_85 = input.LA(1);
						 
						int index13_85 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 94;}
						else if ( (true) ) {s = 31;}
						 
						input.seek(index13_85);
						if ( s>=0 ) return s;
						break;

					case 10 : 
						int LA13_122 = input.LA(1);
						 
						int index13_122 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 127;}
						else if ( (true) ) {s = 11;}
						 
						input.seek(index13_122);
						if ( s>=0 ) return s;
						break;

					case 11 : 
						int LA13_135 = input.LA(1);
						 
						int index13_135 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 137;}
						else if ( (true) ) {s = 11;}
						 
						input.seek(index13_135);
						if ( s>=0 ) return s;
						break;

					case 12 : 
						int LA13_80 = input.LA(1);
						 
						int index13_80 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 89;}
						else if ( (true) ) {s = 31;}
						 
						input.seek(index13_80);
						if ( s>=0 ) return s;
						break;

					case 13 : 
						int LA13_125 = input.LA(1);
						 
						int index13_125 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 129;}
						else if ( (true) ) {s = 11;}
						 
						input.seek(index13_125);
						if ( s>=0 ) return s;
						break;

					case 14 : 
						int LA13_118 = input.LA(1);
						 
						int index13_118 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 123;}
						else if ( (true) ) {s = 11;}
						 
						input.seek(index13_118);
						if ( s>=0 ) return s;
						break;

					case 15 : 
						int LA13_141 = input.LA(1);
						 
						int index13_141 = input.index();
						input.rewind();
						s = -1;
						if ( ((!methodMode)) ) {s = 142;}
						else if ( (true) ) {s = 11;}
						 
						input.seek(index13_141);
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 13, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
