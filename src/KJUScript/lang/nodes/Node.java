package KJUScript.lang.nodes;

import KJUScript.lang.*;

/**
  A node in the AST. Each node can be evaluated.
*/
public abstract class Node implements Evaluable {
}