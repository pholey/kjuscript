package KJUScript.lang.nodes;

import KJUScript.lang.*;

/**
  Get the value of a constant.
*/
public class ConstantNode extends Node {
  private String name;
  
  public ConstantNode(String name) {
    this.name = name;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    return context.getCurrentClass().getConstant(name);
  }
}