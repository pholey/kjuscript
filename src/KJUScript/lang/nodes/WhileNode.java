package KJUScript.lang.nodes;

import KJUScript.lang.*;

public class WhileNode extends Node {
  private Node condition;
  private Node body;
  
  public WhileNode(Node condition, Node body) {
    this.condition = condition;
    this.body = body;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    while (condition.eval(context).isTrue()) {
      body.eval(context);
    }
    return KJUScriptRuntime.getNil();
  }
}