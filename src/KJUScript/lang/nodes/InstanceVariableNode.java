package KJUScript.lang.nodes;

import KJUScript.lang.*;

public class InstanceVariableNode extends Node {
  private String name;
  
  public InstanceVariableNode(String name) {
    this.name = name;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    return context.getCurrentSelf().getInstanceVariable(name);
  }
}