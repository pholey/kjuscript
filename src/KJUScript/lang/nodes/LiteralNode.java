package KJUScript.lang.nodes;

import KJUScript.lang.*;

public class LiteralNode extends Node {
  KJUScriptObject value;
  
  public LiteralNode(KJUScriptObject value) {
    this.value = value;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    return value;
  }
}