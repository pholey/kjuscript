package KJUScript.lang.nodes;

import KJUScript.lang.*;

public class ConstantAssignNode extends Node {
  private String name;
  private Node expression;
  
  public ConstantAssignNode(String name, Node expression) {
    this.name = name;
    this.expression = expression;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    KJUScriptObject value = expression.eval(context);
    context.getCurrentClass().setConstant(name, value);
    return value;
  }
}