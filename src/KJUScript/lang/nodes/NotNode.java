package KJUScript.lang.nodes;

import KJUScript.lang.*;

/**
  Negate a value.
*/
public class NotNode extends Node {
  private Node receiver;
  
  /**
    !receiver
  */
  public NotNode(Node receiver) {
    this.receiver = receiver;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    if (receiver.eval(context).isTrue())
      return KJUScriptRuntime.getFalse();
    return KJUScriptRuntime.getTrue();
  }
}