package KJUScript.lang.nodes;

import KJUScript.lang.*;

public class SelfNode extends Node {
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    return context.getCurrentSelf();
  }
}