package KJUScript.lang.nodes;

import KJUScript.lang.*;

public class LocalAssignNode extends Node {
  private String name;
  private Node expression;
  
  public LocalAssignNode(String name, Node expression) {
    this.name = name;
    this.expression = expression;
  }
  
  public KJUScriptObject eval(Context context) throws KJUScriptException {
    KJUScriptObject value = expression.eval(context);
    context.setLocal(name, value);
    return value;
  }
}