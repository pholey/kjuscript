package KJUScript.lang;

/**
  A method attached to a KJUScriptClass.
*/
public abstract class Method {
  /**
    Calls the method.
    @param receiver  Instance on which to call the method (self).
    @param arguments Arguments passed to the method.
  */
  public abstract KJUScriptObject call(KJUScriptObject receiver, KJUScriptObject arguments[]) throws KJUScriptException;
}
