package KJUScript;

import java.io.Reader;
import java.io.StringReader;
import java.io.FileReader;

import KJUScript.lang.Bootstrapper;

public class Main {
  
  static void Help(){
    
    System.out.println("\nUsage: KJUScript [-d] < -e 'code' | file.kju >\n");
    System.out.println("    -d                Verbose debugging prints");
    System.out.println("    --debug\n");
    System.out.println("    -e                Execute a string of code\n");
    System.out.println("    -h                Brings up this help menu");
    System.out.println("    --help\n\n");
    
  }
  
  public static void main(String[] args) throws Exception {
    String help;
    Reader reader = null;
    boolean debug = false;
    
    for (int i = 0; i < args.length; i++) {
      
      switch (args[i]) {
        
        case "-e":  reader = new StringReader(args[++i]);
                    break;
        
        case "-d":  debug = true;
                    break;

        case "-h":  Help();
                    break;

        case "--debug":  debug = true;
                         break;

        case "--help" :  Help();
                         break;

        default:    reader = new FileReader(args[i]);
                    break;
      
      }
    }
    
    if (reader == null) {
      System.out.println("usage: KJUScript [-d] < -e code | file.kju >");
      System.exit(1);
    }
    
    Bootstrapper.run().eval(reader);
  }
}
